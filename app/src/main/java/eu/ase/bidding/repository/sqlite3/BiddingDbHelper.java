package eu.ase.bidding.repository.sqlite3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_BID_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_ITEM_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.USER_CONTRACT;

public class BiddingDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Bidding.db";
    private static int DATABASE_VERSION = 1;

    BiddingDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // WARNING do not change this order!
        db.execSQL(USER_CONTRACT.SQL.CREATE_TABLE);
        db.execSQL(AUCTION_BID_CONTRACT.SQL.CREATE_TABLE);
        db.execSQL(AUCTION_ITEM_CONTRACT.SQL.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion <= oldVersion) {
            throw new RuntimeException("New version too low");
        }
        db.execSQL(USER_CONTRACT.SQL.DROP_TABLE);
        db.execSQL(AUCTION_BID_CONTRACT.SQL.DROP_TABLE);
        db.execSQL(AUCTION_ITEM_CONTRACT.SQL.DROP_TABLE);

        this.onCreate(db);

        DATABASE_VERSION = newVersion;
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys = ON ;");
        }
    }

    /**
     * Destroys all the data from the SQLite Database
     *
     * @param db Writable SQLite Database instance
     */
    void destroy(SQLiteDatabase db) {
        db.delete(AUCTION_BID_CONTRACT.TABLE_NAME, null, null);
        db.delete(AUCTION_ITEM_CONTRACT.TABLE_NAME, null, null);
        db.delete(USER_CONTRACT.TABLE_NAME, null, null);
    }
}
