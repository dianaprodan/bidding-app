package eu.ase.bidding.repository.sqlite3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import eu.ase.bidding.persistence.Expirable;
import eu.ase.bidding.persistence.Persistable;


public class BiddingDatabase {
    private static SQLiteDatabase readableDb = null;
    private static SQLiteDatabase writableDb = null;
    private static BiddingDbHelper helper = null;

    /**
     * Prevent accidental instantiation
     */
    private BiddingDatabase() {
    }

    /**
     * Initializes SQLite Database.
     * Creates tables and other related stuff.
     * Gets the references to writable and readable database.
     * Must be called only once by the Data Repository.
     *
     * @param context Application context
     */
    public static void init(Context context) {
        helper = new BiddingDbHelper(context);
        readableDb = helper.getReadableDatabase();
        writableDb = helper.getWritableDatabase();
    }

    /**
     * Closes the SQLite Database. Should be called once from the Data Repository
     */
    public static void close() {
        helper.close();
    }

    /**
     * Clear all of the cached data from SQLite Database
     */
    public static void clear() {
        DeletePolicy.DROP_DB.drop();
    }

    /**
     * Stores the model in the SQLite Database
     *
     * @param model Model to write
     * @param table Name of the table where model should be written
     * @param <T>   Model which implements `Persistable`
     * @throws RuntimeException when model couldn't be persisted
     */
    public static <T extends Persistable> void write(@NonNull T model, @NonNull String table) {
        final long id = writableDb.insertWithOnConflict(table, null, model.toContentValues(), SQLiteDatabase.CONFLICT_REPLACE);
        if (id == -1) {
            throw new RuntimeException("Failed to store model");
        }
    }

    /**
     * Retrieves a list of models from SQLiteDatabase
     *
     * @param query         SQL select query
     * @param selectionArgs SQL select query args
     * @param modelType     Class of the model
     * @param <T>           Model which can be constructable from a cursor
     * @return a list of retrieved models
     * @throws RuntimeException when model can't be constructed from Cursor instance or read failed
     */
    static <T> List<T> read(String query, @Nullable String[] selectionArgs, Class<? extends T> modelType) {
        List<T> models = new ArrayList<>();
        try (Cursor cursor = readableDb.rawQuery(query, selectionArgs)) {
            while (cursor.moveToNext()) {
                models.add(modelType.getConstructor(Cursor.class).newInstance(cursor));
            }
        } catch (Exception e) {
            throw new RuntimeException("Model should be constructable from Cursor", e);
        }
        return models;
    }

    /**
     * Retrieves a list of models which have an expiration time from SQLiteDatabase
     *
     * @param selectQuery   SQL select query
     * @param selectionArgs Select query args
     * @param modelType     Type of the model. Must be constructable from Cursor instance
     * @param deletePolicy  Delete policy which needs to be applied when a retrieved model expired
     * @param <T>           Type of the models. Must implement `Expirable` interface.
     * @return A list of retrieved models. List can be empty if no models found, or all models have been expired.
     */
    static <T extends Expirable> List<T> read(String selectQuery, @Nullable String[] selectionArgs, Class<? extends T> modelType, DeletePolicy deletePolicy) {
        List<T> models = new ArrayList<>();
        boolean dropModelsNeedsToBeDone = false;

        try (Cursor cursor = readableDb.rawQuery(selectQuery, selectionArgs)) {
            while (cursor.moveToNext()) {
                T model = modelType.getConstructor(Cursor.class).newInstance(cursor);
                if (model.expired()) {
                    dropModelsNeedsToBeDone = true; // can't delete while iterating over a opened cursor
                    break;
                } else {
                    models.add(model);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to retrieve all of the models", e);
        }

        if (dropModelsNeedsToBeDone) {
            deletePolicy.drop();
            models.clear();
        }

        return models;
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param table       the table to update in
     * @param values      a map from column names to new column values. null is a
     *                    valid value that will be translated to NULL.
     * @param whereClause the optional WHERE clause to apply when updating.
     *                    Passing null will update all rows.
     * @param whereArgs   You may include ?s in the where clause, which
     *                    will be replaced by the values from whereArgs. The values
     *                    will be bound as Strings.
     * @return the number of rows affected
     */
    public static int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return writableDb.updateWithOnConflict(table, values, whereClause, whereArgs, SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * Convenience method for deleting rows in the database.
     *
     * @param table       the table to delete from
     * @param whereClause the optional WHERE clause to apply when deleting.
     *                    Passing null will delete all rows.
     * @param whereArgs   You may include ?s in the where clause, which
     *                    will be replaced by the values from whereArgs. The values
     *                    will be bound as Strings.
     * @return the number of rows affected if a whereClause is passed in, 0
     * otherwise. To remove all rows and get a count pass "1" as the
     * whereClause.
     */
    public static int delete(String table, String whereClause, String[] whereArgs, @Nullable Integer expectedToBeDeleted) {
        int deleted = writableDb.delete(table, whereClause, whereArgs);
        if (expectedToBeDeleted != null && deleted != expectedToBeDeleted) {
            throw new RuntimeException("Not all of the records were deleted");
        }
        return deleted;
    }

    /**
     * Policies for cache invalidation
     */
    public enum DeletePolicy {
        DROP_DB {
            @Override
            public void drop() {
                helper.destroy(writableDb);
            }
        };

        public abstract void drop();
    }

    public static class WriteTransaction implements AutoCloseable {

        WriteTransaction() {
            writableDb.beginTransaction();
        }

        void commit() {
            writableDb.setTransactionSuccessful();
        }

        @Override
        public void close() {
            writableDb.endTransaction();
        }
    }

}
