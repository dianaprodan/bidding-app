package eu.ase.bidding.repository;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import eu.ase.bidding.async.Expect;
import eu.ase.bidding.async.GenericTask;
import eu.ase.bidding.config.ApiConfig;
import eu.ase.bidding.io.network.HttpManager;
import eu.ase.bidding.models.dto.in.AuctionItemDTO;
import eu.ase.bidding.models.dto.in.AuctionItemsResponse;
import eu.ase.bidding.models.dto.out.CheckTokenRequest;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.models.entities.User;
import eu.ase.bidding.repository.preferences.LocalStorage;
import eu.ase.bidding.repository.sqlite3.SQLReader;
import eu.ase.bidding.repository.sqlite3.SQLWriter;

public class DataRetriever {

    private static DataRetriever instance = null;

    private DataRetriever() {
    }

    static synchronized DataRetriever getInstance() {
        if (DataRetriever.instance == null) {
            DataRetriever.instance = new DataRetriever();
        }
        return DataRetriever.instance;
    }

    public void checkToken(final Context context, final OnDataRetrieved<String> onTokenChecked) {
        LocalStorage localStorage = new LocalStorage(context);

        if (!localStorage.has(LocalStorage.Keys.JWT_TOKEN)) {
            onTokenChecked.handle(new Expect<String>(new RuntimeException("Expired token")));
            return;
        }

        final CheckTokenRequest checkTokenRequest = new CheckTokenRequest(localStorage.get(LocalStorage.Keys.JWT_TOKEN));

        new GenericTask<>(new GenericTask.Executable<Void, String>() {
            @Override
            public String execute(Void aVoid) throws Exception {
                final Optional<String> responseBody = HttpManager.getInstance().post(ApiConfig.CHECK_TOKEN_OF_USER, null, checkTokenRequest);
                final JSONObject jsonBody = new JSONObject(responseBody.get());
                return jsonBody.getString("userId");
            }

            @Override
            public void postExecute(Expect<String> result) {
                onTokenChecked.handle(result);
            }
        }).execute();
    }

    public void retrieveAuctionItems(final AuctionItem.Owner owner, final Context context, final OnDataRetrieved<List<AuctionItem>> onItemsRetrievedHandler) {
        new GenericTask<>(new GenericTask.Executable<Void, List<AuctionItem>>() {
            @Override
            public List<AuctionItem> execute(Void aVoid) throws Exception {
                User currentAuthenticatedUser = (User) new LocalStorage(context).get(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, new User());

                List<AuctionItem> auctionItems = SQLReader.readAuctionItems(owner.getSelectSQL(), new String[]{currentAuthenticatedUser.getUsername()});

                if (!auctionItems.isEmpty()) {
                    return auctionItems;
                }

                final Map<String, String> queryParams = new HashMap<>();
                queryParams.put("owners", owner.getOwner());
                String responseBody = HttpManager.getInstance().get(ApiConfig.GET_AUCTION_ITEMS, queryParams);

                JSONObject jsonBody = new JSONObject(responseBody);
                AuctionItemsResponse auctionItemsResponse = (AuctionItemsResponse) new AuctionItemsResponse().fromJSONObject(jsonBody);

                if (auctionItemsResponse.items.isEmpty()) {
                    return auctionItems; // at this moment this list is empty too
                }

                for (AuctionItemDTO auctionItemDTO : auctionItemsResponse.items) {
                    auctionItems.add(new AuctionItem(auctionItemDTO));
                }

                SQLWriter.write(auctionItems);

                return auctionItems;
            }

            @Override
            public void postExecute(Expect<List<AuctionItem>> result) {
                onItemsRetrievedHandler.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public User retrieveCurrentAuthenticatedUser(Context context) throws JSONException {
        return (User) new LocalStorage(context).get(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, new User());
    }

    public interface OnDataRetrieved<T> {
        void handle(Expect<T> data);
    }
}
