package eu.ase.bidding.repository;

import android.content.ContentValues;
import android.content.Context;

import androidx.annotation.IdRes;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import eu.ase.bidding.async.Expect;
import eu.ase.bidding.async.GenericTask;
import eu.ase.bidding.config.ApiConfig;
import eu.ase.bidding.io.network.HttpManager;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.Wallet;
import eu.ase.bidding.models.dto.in.AuthenticationResponse;
import eu.ase.bidding.models.dto.in.ResourceIdResponse;
import eu.ase.bidding.models.dto.out.AuctionItemDTO;
import eu.ase.bidding.models.dto.out.AuthenticationRequest;
import eu.ase.bidding.models.dto.out.UserDTO;
import eu.ase.bidding.models.entities.AuctionBid;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.models.entities.User;
import eu.ase.bidding.repository.firebase.FirebaseWalletController;
import eu.ase.bidding.repository.preferences.LocalStorage;
import eu.ase.bidding.repository.sqlite3.BiddingDatabase;
import eu.ase.bidding.repository.sqlite3.SQLWriter;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_BID_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_ITEM_CONTRACT;
import eu.ase.bidding.utils.CryptoUtils;
import eu.ase.bidding.utils.StringUtils;

public class DataPersister {
    private static DataPersister instance = null;

    /**
     * Singleton pattern
     */
    private DataPersister() {
    }

    /**
     * Singleton. Can be called safely by multiple threads
     *
     * @return Instance
     */
    static synchronized DataPersister getInstance() {
        if (instance == null) {
            instance = new DataPersister();
        }
        return instance;
    }

    /**
     * Registers a new user, then creates an empty wallet for that user.
     *
     * @param userDTO          User registration info
     * @param context          Context of the caller
     * @param onUserRegistered Callback when user was registered
     */
    public void registerUser(final UserDTO userDTO, final Context context, final OnDataPersisted<Boolean> onUserRegistered) {
        new GenericTask<>(new GenericTask.Executable<Void, Boolean>() {
            @Override
            public Boolean execute(Void aVoid) throws Exception {
                HttpManager.getInstance().post(ApiConfig.REGISTER_USER, null, userDTO);
                FirebaseWalletController.getInstance().createUserWallet(new User(userDTO).getId());
                return true;
            }

            @Override
            public void postExecute(Expect<Boolean> result) {
                onUserRegistered.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    /**
     * Authenticates the user using Basic HTTP Authentication.
     * Stores JWT and details about authenticated user in local storage of the device.
     * Syncs wallet of the authenticated user with the wallet from remote Firebase realtime database.
     *
     * @param authenticationRequest    Authentication Request which contains user credentials
     * @param context                  Application context
     * @param onAuthenticationComplete Callback when authentication has been completed or failed.
     */
    public void authenticateUser(final AuthenticationRequest authenticationRequest, final Context context, final OnDataPersisted<Boolean> onAuthenticationComplete) {
        new GenericTask<>(new GenericTask.Executable<Void, Boolean>() {
            @Override
            public Boolean execute(Void aVoid) throws Exception {
                final Optional<String> responseBody = HttpManager.getInstance().post(ApiConfig.AUTHENTICATE_USER, null, authenticationRequest);

                final JSONObject jsonBody = new JSONObject(responseBody.get()); // it must return auth token
                final AuthenticationResponse authenticationResponse = (AuthenticationResponse) new AuthenticationResponse().fromJSONObject(jsonBody);

                final User authenticatedUser = new User(authenticationResponse.user);

                final LocalStorage localStorage = new LocalStorage(context);
                localStorage.store(LocalStorage.Keys.JWT_TOKEN, authenticationResponse.jwt);
                localStorage.store(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, authenticatedUser);

                FirebaseWalletController.getInstance().syncUserWallet(authenticatedUser.getId(), context);

                return true;
            }

            @Override
            public void postExecute(Expect<Boolean> result) {
                onAuthenticationComplete.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public void createAuctionItem(final AuctionItemDTO auctionItemOutDTO, final Context context, final OnDataPersisted<AuctionItem> onItemPersisted) {
        new GenericTask<>(new GenericTask.Executable<Void, AuctionItem>() {
            @Override
            public AuctionItem execute(Void aVoid) throws Exception {
                Optional<String> response = HttpManager.getInstance().post(ApiConfig.CREATE_AUCTION_ITEM, null, auctionItemOutDTO);

                ResourceIdResponse auctionItemId = (ResourceIdResponse) new ResourceIdResponse().fromJSONObject(new JSONObject(response.get()));
                User currentAuthenticatedUser = (User) new LocalStorage(context).get(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, new User());
                AuctionItem auctionItem = new AuctionItem(auctionItemId, currentAuthenticatedUser, new ArrayList<AuctionBid>(), auctionItemOutDTO);

                SQLWriter.write(auctionItem);

                return auctionItem;
            }

            @Override
            public void postExecute(Expect<AuctionItem> result) {
                onItemPersisted.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public void deleteAuctionItem(@IdRes final Long id, final Context context, final OnDataPersisted<Boolean> onItemDeleted) {
        new GenericTask<>(new GenericTask.Executable<Void, Boolean>() {
            @Override
            public Boolean execute(Void aVoid) throws Exception {
                String itemId = id.toString();

                Map<String, String> queryParams = new HashMap<>();
                queryParams.put("auctionItemId", itemId);
                HttpManager.getInstance().delete(ApiConfig.DELETE_AUCTION_ITEM, queryParams, null);

                String whereClause = AUCTION_ITEM_CONTRACT.ENTRY._ID + " = ? ";
                String[] whereArgs = new String[]{itemId};
                BiddingDatabase.delete(AUCTION_ITEM_CONTRACT.TABLE_NAME, whereClause, whereArgs, null);

                return true;
            }

            @Override
            public void postExecute(Expect<Boolean> result) {
                onItemDeleted.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public void changeCurrentAuctionItemStatus(final AuctionItem auctionItem, final AuctionItemStatus auctionItemStatus, final Context context, final OnDataPersisted<Boolean> onItemPlacedInAuction) {
        new GenericTask<>(new GenericTask.Executable<Void, Boolean>() {
            @Override
            public Boolean execute(Void aVoid) throws Exception {
                if (auctionItemStatus.status == AuctionItemStatus.Status.SOLD_OUT) {
                    AuctionBid lastBid = auctionItem.getLastBid();
                    if (lastBid == null) {
                        throw new RuntimeException("Can't sold out item which has no bids");
                    }

                    // get wallet for current user
                    LocalStorage localStorage = new LocalStorage(context);
                    User currentAuthenticatedUser = (User) localStorage.get(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, new User());
                    Wallet walletOfCurrentAuthenticatedUser = (Wallet) localStorage.get(LocalStorage.Keys.WALLET_OF_CURRENT_AUTHENTICATED_USER, new Wallet());

                    // get wallet for the user who did the last bid
                    String lastBidderId = CryptoUtils.md5(lastBid.getBidderId()); // FIXME broken encapsulation
                    Wallet lastBidderWallet = FirebaseWalletController.getInstance().readUserWallet(lastBidderId).await().getResult();

                    // withdraw amount from the wallet of last bidder
                    Wallet.Transaction withdrawTransaction = lastBidderWallet.withdraw(lastBid.getAmount(), auctionItem.getCurrency());
                    FirebaseWalletController.getInstance().updateUserWallet(lastBidderId, auctionItem.getCurrency(), withdrawTransaction.newAmount);
                    withdrawTransaction.commit();

                    try {
                        // deposit amount to current user
                        Wallet.Transaction depositTransaction = walletOfCurrentAuthenticatedUser.deposit(lastBid.getAmount(), auctionItem.getCurrency());
                        FirebaseWalletController.getInstance().updateUserWallet(currentAuthenticatedUser.getId(), auctionItem.getCurrency(), depositTransaction.newAmount);
                        depositTransaction.commit();
                    } catch (Exception e) {
                        Wallet.Transaction depositBackTransaction = lastBidderWallet.deposit(lastBid.getAmount(), auctionItem.getCurrency());
                        FirebaseWalletController.getInstance().updateUserWallet(lastBidderId, auctionItem.getCurrency(), depositBackTransaction.newAmount);
                        depositBackTransaction.commit();

                        throw e;
                    }

                    // update local wallet
                    localStorage.store(LocalStorage.Keys.WALLET_OF_CURRENT_AUTHENTICATED_USER, walletOfCurrentAuthenticatedUser);
                }

                final String auctionItemIdString = auctionItem.getId().toString();

                final Map<String, String> queryParams = new HashMap<>();
                queryParams.put("auctionItemId", auctionItemIdString);
                HttpManager.getInstance().put(ApiConfig.CHANGE_ITEM_STATUS, queryParams, auctionItemStatus);

                final ContentValues valuesToUpdate = new ContentValues();
                valuesToUpdate.put(AUCTION_ITEM_CONTRACT.ENTRY.CURRENT_STATUS, auctionItemStatus.status.toString());
                valuesToUpdate.put(AUCTION_ITEM_CONTRACT.ENTRY.TIMESTAMP_FOR_STATUS, auctionItemStatus.timestamp.getTime());
                final String whereClause = AUCTION_ITEM_CONTRACT.ENTRY._ID + " = ?";
                final String[] whereArgs = new String[]{auctionItemIdString};
                final int updatedItems = BiddingDatabase.update(AUCTION_ITEM_CONTRACT.TABLE_NAME, valuesToUpdate, whereClause, whereArgs);

                if (updatedItems != 1) {
                    throw new RuntimeException("Not all of the items where updated in SQLite Database");
                }

                return true;
            }

            @Override
            public void postExecute(Expect<Boolean> result) {
                onItemPlacedInAuction.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public void persistBid(@IdRes final Long auctionItemId, final Double amount, final Currency currency, final Context context, final OnDataPersisted<AuctionBid> onBidAdded) {
        new GenericTask<>(new GenericTask.Executable<Void, AuctionBid>() {
            @Override
            public AuctionBid execute(Void aVoid) throws Exception {
                LocalStorage localStorage = new LocalStorage(context);
                Wallet walletOfCurrentAuthenticatedUser = (Wallet) localStorage.get(LocalStorage.Keys.WALLET_OF_CURRENT_AUTHENTICATED_USER, new Wallet());
                if (!walletOfCurrentAuthenticatedUser.has(amount, currency)) {
                    throw new RuntimeException("Not enough balance");
                }

                final Map<String, String> queryParams = new HashMap<>();
                queryParams.put("auctionItemId", String.format(Locale.ENGLISH, "%d", auctionItemId));
                queryParams.put("amount", String.format(Locale.ENGLISH, "%.2f", amount));

                final Optional<String> responseBody = HttpManager.getInstance().put(ApiConfig.ADD_BID_FOR_AUCTION_ITEM, queryParams, null);
                final JSONObject jsonBody = new JSONObject(responseBody.get()); // it must send back id of the bid
                final ResourceIdResponse bidIdResponse = (ResourceIdResponse) new ResourceIdResponse().fromJSONObject(jsonBody);

                final User currentAuthenticatedUser = (User) new LocalStorage(context).get(LocalStorage.Keys.CURRENT_AUTHENTICATED_USER, new User());
                final AuctionBid auctionBid = new AuctionBid(bidIdResponse, currentAuthenticatedUser.getUsername(), auctionItemId, amount);

                BiddingDatabase.write(auctionBid, AUCTION_BID_CONTRACT.TABLE_NAME);

                return auctionBid;
            }

            @Override
            public void postExecute(Expect<AuctionBid> result) {
                onBidAdded.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    public void persistRatingStars(final AuctionItem auctionItem, final Float stars, final Context context, final OnDataPersisted<Float> onRatingAdded) {
        new GenericTask<>(new GenericTask.Executable<Void, Float>() {
            @Override
            public Float execute(Void aVoid) throws Exception {
                String auctionItemIdString = String.format(Locale.ENGLISH, "%d", auctionItem.getId());
                String ratingString = String.format(Locale.ENGLISH, "%1$,.1f", stars);

                final Map<String, String> queryParams = new HashMap<>();
                queryParams.put("auctionItemId", auctionItemIdString);
                queryParams.put("rating", ratingString);

                HttpManager.getInstance().put(ApiConfig.ADD_RATING_FOR_AUCTION_ITEM, queryParams, null);

                String updatedCsvRating = StringUtils.append(auctionItem.ratingToString(), ratingString, ",");
                ContentValues contentToUpdate = new ContentValues();
                contentToUpdate.put(AUCTION_ITEM_CONTRACT.ENTRY.RATING, updatedCsvRating);
                String whereClause = AUCTION_ITEM_CONTRACT.ENTRY._ID + " = ?";
                String[] whereArgs = new String[]{auctionItemIdString};
                BiddingDatabase.update(AUCTION_ITEM_CONTRACT.TABLE_NAME, contentToUpdate, whereClause, whereArgs);

                return stars;
            }

            @Override
            public void postExecute(Expect<Float> result) {
                onRatingAdded.handle(result);
                DataRepository.postHandle(result, context);
            }
        }).execute();
    }

    /**
     * Callback interface
     */
    public interface OnDataPersisted<T> {
        /**
         * Should handle item which was persisted
         */
        void handle(Expect<T> result);
    }
}
