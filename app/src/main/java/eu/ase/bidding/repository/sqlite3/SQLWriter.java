package eu.ase.bidding.repository.sqlite3;

import java.util.List;

import eu.ase.bidding.models.entities.AuctionBid;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_BID_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_ITEM_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.USER_CONTRACT;

public class SQLWriter {
    private SQLWriter() {
    }

    public static void write(AuctionItem auctionItem) {
        BiddingDatabase.write(auctionItem.getOwner(), USER_CONTRACT.TABLE_NAME);
        BiddingDatabase.write(auctionItem, AUCTION_ITEM_CONTRACT.TABLE_NAME);
        for (AuctionBid bid : auctionItem.getBids()) {
            BiddingDatabase.write(bid, AUCTION_BID_CONTRACT.TABLE_NAME);
        }
    }

    public static void write(List<AuctionItem> auctionItems) {
        try (BiddingDatabase.WriteTransaction transaction = new BiddingDatabase.WriteTransaction()) {
            for (AuctionItem item : auctionItems) {
                write(item);
            }
            transaction.commit();
        }
    }
}
