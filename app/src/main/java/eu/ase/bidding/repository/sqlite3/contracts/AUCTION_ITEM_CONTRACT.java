package eu.ase.bidding.repository.sqlite3.contracts;

import android.provider.BaseColumns;

import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.utils.EnumUtils;

public class AUCTION_ITEM_CONTRACT {
    public static final String TABLE_NAME = "auctionitem";

    private AUCTION_ITEM_CONTRACT() {
    }

    public static final class ENTRY implements BaseColumns {
        public static final String NAME = "name";
        public static final String OWNER = "owner";
        public static final String DESCRIPTION = "description";
        public static final String RATING = "rating";
        public static final String CURRENCY = "currency";
        public static final String STARTING_PRICE = "startingPrice";
        public static final String IMAGE = "image";
        public static final String CURRENT_STATUS = "currentStatus";
        public static final String TIMESTAMP_FOR_STATUS = "timestampForStatus";
        public static final String STORED_AT = "storedAt";
        public static final String COORD_LAT = "latitude";
        public static final String COORD_LONG = "longitude";
    }

    public static final class SQL {
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        public static final String GET_FOR_CURRENT_USER =
                "SELECT " + TABLE_NAME + "." + ENTRY._ID + ", "
                        + TABLE_NAME + "." + ENTRY.NAME + ", "
                        + TABLE_NAME + "." + ENTRY.DESCRIPTION + ", "
                        + TABLE_NAME + "." + ENTRY.RATING + ", "
                        + TABLE_NAME + "." + ENTRY.CURRENCY + ", "
                        + TABLE_NAME + "." + ENTRY.STARTING_PRICE + ", "
                        + TABLE_NAME + "." + ENTRY.IMAGE + ", "
                        + TABLE_NAME + "." + ENTRY.CURRENT_STATUS + ", "
                        + TABLE_NAME + "." + ENTRY.TIMESTAMP_FOR_STATUS + ", "
                        + TABLE_NAME + "." + ENTRY.STORED_AT + ", "
                        + TABLE_NAME + "." + ENTRY.COORD_LAT + ", "
                        + TABLE_NAME + "." + ENTRY.COORD_LONG + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.USERNAME + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.MOBILE_NUMBER + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.EMAIL + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.ADDRESS + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.NAME + " "
                        + "FROM " + TABLE_NAME + " INNER JOIN " + USER_CONTRACT.TABLE_NAME + " "
                        + "ON " + TABLE_NAME + "." + ENTRY.OWNER + " = " + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.USERNAME + " "
                        + "WHERE " + TABLE_NAME + "." + ENTRY.OWNER + " = ? "
                        + "GROUP BY " + TABLE_NAME + "." + ENTRY._ID + ";";
        public static final String GET_FOR_OTHER_USERS =
                "SELECT " + TABLE_NAME + "." + ENTRY._ID + ", "
                        + TABLE_NAME + "." + ENTRY.NAME + ", "
                        + TABLE_NAME + "." + ENTRY.DESCRIPTION + ", "
                        + TABLE_NAME + "." + ENTRY.RATING + ", "
                        + TABLE_NAME + "." + ENTRY.CURRENCY + ", "
                        + TABLE_NAME + "." + ENTRY.STARTING_PRICE + ", "
                        + TABLE_NAME + "." + ENTRY.IMAGE + ", "
                        + TABLE_NAME + "." + ENTRY.CURRENT_STATUS + ", "
                        + TABLE_NAME + "." + ENTRY.TIMESTAMP_FOR_STATUS + ", "
                        + TABLE_NAME + "." + ENTRY.STORED_AT + ", "
                        + TABLE_NAME + "." + ENTRY.COORD_LAT + ", "
                        + TABLE_NAME + "." + ENTRY.COORD_LONG + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.USERNAME + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.MOBILE_NUMBER + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.EMAIL + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.ADDRESS + ", "
                        + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.NAME + " "
                        + "FROM " + TABLE_NAME + " INNER JOIN " + USER_CONTRACT.TABLE_NAME + " "
                        + "ON " + TABLE_NAME + "." + ENTRY.OWNER + " = " + USER_CONTRACT.TABLE_NAME + "." + USER_CONTRACT.ENTRY.USERNAME + " "
                        + "WHERE " + TABLE_NAME + "." + ENTRY.OWNER + " != ? "
                        + "GROUP BY " + TABLE_NAME + "." + ENTRY._ID + ";";
        public static String CREATE_TABLE;

        static {
            String currenciesValues = EnumUtils.getNamesCSV(Currency.class, ",", "'");
            String statusValues = EnumUtils.getNamesCSV(AuctionItemStatus.Status.class, ",", "'");

            CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
                    + TABLE_NAME + " ("
                    + ENTRY._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ENTRY.NAME + " TEXT NOT NULL, "
                    + ENTRY.OWNER + " TEXT NOT NULL, "
                    + ENTRY.DESCRIPTION + " TEXT NOT NULL, "
                    + ENTRY.RATING + " TEXT NOT NULL,"
                    + ENTRY.CURRENCY + " TEXT CHECK ( " + ENTRY.CURRENCY + " IN ( " + currenciesValues + " ) ) NOT NULL, "
                    + ENTRY.STARTING_PRICE + " REAL NOT NULL, "
                    + ENTRY.IMAGE + " BLOB NOT NULL, "
                    + ENTRY.CURRENT_STATUS + " TEXT CHECK ( " + ENTRY.CURRENT_STATUS + " IN ( " + statusValues + " ) ) NOT NULL, "
                    + ENTRY.TIMESTAMP_FOR_STATUS + " INTEGER NOT NULL, "
                    + ENTRY.STORED_AT + " INTEGER NOT NULL, "
                    + ENTRY.COORD_LAT + " REAL NOT NULL, "
                    + ENTRY.COORD_LONG + " REAL NOT NULL, "
                    + "FOREIGN KEY (" + ENTRY.OWNER + ") REFERENCES " +
                    USER_CONTRACT.TABLE_NAME + " (" + USER_CONTRACT.ENTRY.USERNAME + ")"
                    + ");";
        }
    }
}
