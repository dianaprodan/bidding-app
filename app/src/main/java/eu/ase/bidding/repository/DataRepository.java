package eu.ase.bidding.repository;

import android.content.Context;

import eu.ase.bidding.async.Expect;
import eu.ase.bidding.repository.preferences.LocalStorage;
import eu.ase.bidding.repository.sqlite3.BiddingDatabase;
import eu.ase.bidding.utils.FileUtils;

public class DataRepository {
    public static Interceptor resultInterceptor = null;

    private DataRepository() {
    }

    public static void open(Context context) {
        BiddingDatabase.init(context);
    }

    public static void close() {
        BiddingDatabase.close();
    }

    public static void clear(Context context) {
        new LocalStorage(context).clear();
        BiddingDatabase.clear();
        FileUtils.deleteTempFiles(context.getCacheDir());
    }

    public static DataRetriever retriever() {
        return DataRetriever.getInstance();
    }

    public static DataPersister persister() {
        return DataPersister.getInstance();
    }

    static void postHandle(Expect<?> result, Context context) {
        if (resultInterceptor != null) {
            resultInterceptor.postHandle(result, context);
        }
    }

    public abstract static class Interceptor {
        /**
         * Method which is executed after the async result was handled by the caller.
         * Usually there is placed common logic for handling specific errors, such as network or
         * authentication exceptions. It is executed after main handler, because it is possible
         * that main handler needs to do some clean up operations.
         *
         * @param result  Result of an async computation
         * @param context Context of the caller which started async computation
         */
        public abstract void postHandle(Expect<?> result, Context context);
    }
}
