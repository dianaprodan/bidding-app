package eu.ase.bidding.repository.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

import eu.ase.bidding.persistence.Jsonable;

public class LocalStorage {

    private Context appContext;

    public LocalStorage(Context appContext) {
        this.appContext = appContext;
    }

    public void store(String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void store(String key, Jsonable jsonable) throws JSONException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, jsonable.toJSONObject().toString());
        editor.apply();
    }

    public boolean has(String key) {
        return PreferenceManager.getDefaultSharedPreferences(appContext).contains(key);
    }

    public Jsonable get(String key, Jsonable jsonable) throws JSONException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        String jsonObjectString = sharedPreferences.getString(key, null);
        if (jsonObjectString == null) {
            throw new RuntimeException(String.format("Key %s not found", key));
        }
        return jsonable.fromJSONObject(new JSONObject(jsonObjectString));
    }

    public <T> T get(String key, Class<T> type) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        String jsonObjectString = sharedPreferences.getString(key, null);
        if (jsonObjectString == null) {
            throw new RuntimeException(String.format("Key %s not found", key));
        }
        return type.getConstructor(JSONObject.class).newInstance(new JSONObject(jsonObjectString));
    }

    public String get(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);
        String stringValue = sharedPreferences.getString(key, null);
        if (stringValue == null) {
            throw new RuntimeException(String.format("Key %s not found", key));
        }
        return stringValue;
    }

    public void delete(String key) {
        PreferenceManager.getDefaultSharedPreferences(appContext).edit().remove(key).apply();
    }

    public void clear() {
        PreferenceManager.getDefaultSharedPreferences(appContext).edit().clear().apply();
    }

    public static class Keys {
        public static final String CURRENT_AUTHENTICATED_USER = "CURRENT_AUTHENTICATED_USER";
        public static final String WALLET_OF_CURRENT_AUTHENTICATED_USER = "WALLET_OF_CURRENT_AUTHENTICATED_USER";
        public static final String JWT_TOKEN = "JWT_TOKEN";
    }
}
