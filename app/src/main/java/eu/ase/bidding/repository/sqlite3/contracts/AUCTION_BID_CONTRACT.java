package eu.ase.bidding.repository.sqlite3.contracts;

import android.provider.BaseColumns;

public class AUCTION_BID_CONTRACT {
    public static final String TABLE_NAME = "auctionbid";

    private AUCTION_BID_CONTRACT() {
    }

    public static final class ENTRY implements BaseColumns {
        public static final String BIDDER_ID = "bidderId";
        public static final String AUCTION_ITEM_ID = "auctionItemId";
        public static final String AMOUNT = "amount";
        public static final String TIMESTAMP = "timestamp";
    }

    public static final class SQL {
        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME + " ("
                + ENTRY._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ENTRY.BIDDER_ID + " TEXT NOT NULL, "
                + ENTRY.AUCTION_ITEM_ID + " INTEGER NOT NULL, "
                + ENTRY.AMOUNT + " REAL NOT NULL, "
                + ENTRY.TIMESTAMP + " INTEGER NOT NULL, "
                + "FOREIGN KEY (" + ENTRY.AUCTION_ITEM_ID + ") REFERENCES " +
                AUCTION_ITEM_CONTRACT.TABLE_NAME + " (" + AUCTION_ITEM_CONTRACT.ENTRY._ID + ") ON DELETE CASCADE "
                + ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

        public static final String BIDS_FOR_AUCTION_ITEM =
                "SELECT * FROM " + TABLE_NAME + " WHERE " + ENTRY.AUCTION_ITEM_ID + " = ?;";
    }
}
