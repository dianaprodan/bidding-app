package eu.ase.bidding.repository.sqlite3;

import androidx.annotation.Nullable;

import java.util.List;

import eu.ase.bidding.models.entities.AuctionBid;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_BID_CONTRACT;

public class SQLReader {
    private SQLReader() {
    }

    public static List<AuctionItem> readAuctionItems(String selectSql, @Nullable String[] whereArgs) {
        List<AuctionItem> auctionItems =
                BiddingDatabase.read(selectSql, whereArgs, AuctionItem.class, BiddingDatabase.DeletePolicy.DROP_DB);
        for (AuctionItem auctionItem : auctionItems) {
            final String auctionBidsSql = AUCTION_BID_CONTRACT.SQL.BIDS_FOR_AUCTION_ITEM;
            final String[] auctionItemIdArgs = new String[]{auctionItem.getId().toString()};
            List<AuctionBid> bids = BiddingDatabase.read(auctionBidsSql, auctionItemIdArgs, AuctionBid.class);
            auctionItem.addBids(bids);
        }
        return auctionItems;
    }
}
