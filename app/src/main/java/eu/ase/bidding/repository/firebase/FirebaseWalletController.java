package eu.ase.bidding.repository.firebase;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import eu.ase.bidding.async.Promise;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.Wallet;
import eu.ase.bidding.repository.preferences.LocalStorage;

public class FirebaseWalletController {
    private static final String WALLETS_CHILD = "wallets";
    private static FirebaseWalletController instance = null;
    private DatabaseReference dbRef;

    private FirebaseWalletController() {
        dbRef = FirebaseDatabase.getInstance().getReference();
    }

    public static synchronized FirebaseWalletController getInstance() {
        if (instance == null) {
            instance = new FirebaseWalletController();
        }
        return instance;
    }

    public void createUserWallet(String userId) {
        Wallet wallet = new Wallet();
        wallet.deposit(100d, Currency.EUR).commit();
        wallet.deposit(100d, Currency.USD).commit();
        dbRef.child(WALLETS_CHILD)
                .child(userId)
                .setValue(wallet);
    }

    public void updateUserWallet(String userId, Currency currency, Double amount) {
        dbRef.child(WALLETS_CHILD)
                .child(userId)
                .child(Wallet.BALANCE_FIREBASE_JSON_KEY)
                .child(currency.toString())
                .setValue(amount);
    }

    @SuppressWarnings("unused")
    public void deleteUserWallet(String userId) {
        dbRef.child(WALLETS_CHILD)
                .child(userId)
                .removeValue();
    }

    public void syncUserWallet(final String userId, final Context context) {
        dbRef.child(WALLETS_CHILD).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    final Wallet wallet = Objects.requireNonNull(dataSnapshot.getValue(Wallet.class));
                    final LocalStorage localStorage = new LocalStorage(context);
                    localStorage.store(LocalStorage.Keys.WALLET_OF_CURRENT_AUTHENTICATED_USER, wallet);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.err.println(databaseError);
            }
        });
    }

    public Promise<Wallet> readUserWallet(final String userId) {
        final Promise<Wallet> walletPromise = new Promise<>();

        dbRef.child(WALLETS_CHILD).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    walletPromise.resolve(Objects.requireNonNull(dataSnapshot.getValue(Wallet.class)));
                } catch (Exception e) {
                    e.printStackTrace();
                    walletPromise.reject(e);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                walletPromise.reject(databaseError.toException());
            }
        });

        return walletPromise;
    }
}
