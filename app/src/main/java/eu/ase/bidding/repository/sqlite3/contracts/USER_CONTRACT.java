package eu.ase.bidding.repository.sqlite3.contracts;

import android.provider.BaseColumns;

public final class USER_CONTRACT {
    public static final String TABLE_NAME = "user";

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private USER_CONTRACT() {
    }

    /* Inner class that defines the table contents */
    public static class ENTRY implements BaseColumns {
        public static final String USERNAME = "username";
        public static final String NAME = "name";
        public static final String ADDRESS = "address";
        public static final String EMAIL = "email";
        public static final String MOBILE_NUMBER = "mobile";
    }

    public static class SQL {
        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME + " ("
                + ENTRY.USERNAME + " TEXT PRIMARY KEY, "
                + ENTRY.NAME + " TEXT NOT NULL, "
                + ENTRY.ADDRESS + " TEXT NOT NULL, "
                + ENTRY.EMAIL + " TEXT NOT NULL, "
                + ENTRY.MOBILE_NUMBER + " TEXT NOT NULL);";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }
}
