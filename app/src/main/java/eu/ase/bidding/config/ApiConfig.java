package eu.ase.bidding.config;

public class ApiConfig {
    public static final String BASE_URL = "http://192.168.43.242:8080/"; // FIXME  to be changed
    public static final String USED_API = "api/rest/latest/";
    public static final String AUTHENTICATE_USER = BASE_URL + USED_API + "authenticate";
    public static final String REGISTER_USER = BASE_URL + USED_API + "register";
    public static final String CHECK_TOKEN_OF_USER = BASE_URL + USED_API + "checkToken";
    public static final String GET_AUCTION_ITEMS = BASE_URL + USED_API + "auctionItems";
    public static final String CREATE_AUCTION_ITEM = BASE_URL + USED_API + "auctionItems";
    public static final String DELETE_AUCTION_ITEM = BASE_URL + USED_API + "auctionItems";
    public static final String CHANGE_ITEM_STATUS = BASE_URL + USED_API + "auctionItems/status/change";
    public static final String ADD_BID_FOR_AUCTION_ITEM = BASE_URL + USED_API + "auctionItems/bids/add";
    public static final String ADD_RATING_FOR_AUCTION_ITEM = BASE_URL + USED_API + "auctionItems/rating/add";

    private ApiConfig() {
    }
}
