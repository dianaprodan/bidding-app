package eu.ase.bidding.config;

public class AppConfig {
    public static final long NUM_BYTES_NEEDED_FOR_FILES = 1024 * 1024 * 10L; // 10 MB

    private AppConfig() {
    }
}
