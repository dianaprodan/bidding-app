package eu.ase.bidding.config;

public class Models {

    private Models() {
    }

    public static class TTL {
        public static final int AUCTION_ITEM = 900 * 1000; // 15 minutes
    }

    public static final double WALLET_INITIAL_BALANCE = 10000;
}
