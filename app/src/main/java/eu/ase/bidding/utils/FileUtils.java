package eu.ase.bidding.utils;

import android.content.Context;
import android.content.Intent;
import android.os.storage.StorageManager;
import android.util.Xml;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import eu.ase.bidding.config.AppConfig;
import eu.ase.bidding.persistence.Xmlable;

import static android.os.storage.StorageManager.ACTION_MANAGE_STORAGE;

public class FileUtils {
    private FileUtils() {
    }

    public static boolean deviceHasNoAvailableSpace(Context appContext) throws IOException {
        StorageManager storageManager =
                appContext.getSystemService(StorageManager.class);
        assert storageManager != null;
        UUID appSpecificInternalDirUuid = storageManager.getUuidForPath(appContext.getFilesDir());
        long availableBytes =
                storageManager.getAllocatableBytes(appSpecificInternalDirUuid);
        if (availableBytes >= AppConfig.NUM_BYTES_NEEDED_FOR_FILES) {
            storageManager.allocateBytes(
                    appSpecificInternalDirUuid, AppConfig.NUM_BYTES_NEEDED_FOR_FILES);
            return false;
        } else {
            Intent storageIntent = new Intent();
            // Display prompt to user, requesting that they choose files to remove.
            storageIntent.setAction(ACTION_MANAGE_STORAGE);
            return true;
        }
    }

    public static void write(File file, byte[] content, Context context) throws IOException {
        try (FileOutputStream fos = context.openFileOutput(file.getName(), Context.MODE_PRIVATE)) {
            fos.write(content);
        }
    }

    public static void write(File file, Xmlable xmlable) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            XmlSerializer serializer = Xml.newSerializer();

            serializer.setOutput(fileOutputStream, "UTF-8");
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            xmlable.serialize(serializer);

            serializer.endDocument();
            serializer.flush();
        }
    }

    public static String read(File file, Context context) throws IOException {
        FileInputStream fileInputStream = context.openFileInput(file.getName());
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
        try (BufferedReader reader = new BufferedReader(inputStreamReader)) {

            StringBuilder stringBuilder = new StringBuilder();

            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = reader.readLine();
            }

            return stringBuilder.toString();
        }
    }

    public static Document readXML(File file) throws ParserConfigurationException, IOException, SAXException {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

            documentBuilderFactory.setNamespaceAware(false);
            documentBuilderFactory.setValidating(true);

            DocumentBuilder db = documentBuilderFactory.newDocumentBuilder();

            return db.parse(new InputSource(fileInputStream));
        }
    }

    public static void deleteTempFiles(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        deleteTempFiles(f);
                    } else {
                        if (!f.delete()) {
                            throw new RuntimeException("Failed to delete " + f.getName());
                        }
                    }
                }
            }
        }
        if (!file.delete()) {
            throw new RuntimeException("Failed to delete " + file.getName());
        }
    }
}
