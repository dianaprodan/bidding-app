package eu.ase.bidding.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ConverterUtils {

    public static Date dateFromISOString(String isoDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH).parse(isoDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
