package eu.ase.bidding.utils;

import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EnumUtils {
    public static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").split(", ");
    }

    public static String getNamesCSV(Class<? extends Enum<?>> e, String joiner, @Nullable final String quotes) {
        List<String> enumValues = Arrays.asList(getNames(e));

        if (quotes != null) {
            enumValues = enumValues.stream().map(new Function<String, String>() {
                @Override
                public String apply(String enumValue) {
                    return quotes + enumValue + quotes;
                }
            }).collect(Collectors.<String>toList());
        }

        return StringUtils.join(enumValues, joiner);
    }
}
