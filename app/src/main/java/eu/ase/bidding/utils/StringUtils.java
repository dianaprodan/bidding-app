package eu.ase.bidding.utils;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
    private StringUtils() {
    }

    public static <T> String join(List<T> listOfValues, String joiner) {
        StringBuilder stringBuilder = new StringBuilder();
        String separator = "";

        for (T value : listOfValues) {
            stringBuilder.append(separator);
            separator = joiner;
            stringBuilder.append(value);
        }

        return stringBuilder.toString();
    }

    public static <T> String append(String csvString, T value, String delimiter) {
        return csvString.isEmpty() ? value.toString() : csvString + delimiter + value;
    }

    public static List<Float> splitIntoFloats(String string, String splitter) {
        String[] stringNumbers = string.split(splitter);
        List<Float> numbers = new ArrayList<>();
        for (String stringNumber : stringNumbers) {
            if (!stringNumber.isEmpty()) {
                numbers.add(Float.parseFloat(stringNumber));
            }
        }
        return numbers;
    }

    @SuppressWarnings("unused")
    public static String capitalize(String string, boolean toLowerCase) {
        String stringToCapitalize = toLowerCase ? string.toLowerCase() : string;
        return stringToCapitalize.substring(0, 1).toUpperCase() + stringToCapitalize.substring(1);
    }

    public static String capitalize(Enum<?> enumValue) {
        String stringToCapitalize = enumValue.toString().toLowerCase();
        stringToCapitalize = stringToCapitalize.replace('_', ' ');
        return stringToCapitalize.substring(0, 1).toUpperCase() + stringToCapitalize.substring(1);
    }

}
