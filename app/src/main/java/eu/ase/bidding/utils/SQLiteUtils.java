package eu.ase.bidding.utils;

import android.database.Cursor;

import java.util.Date;

public class SQLiteUtils {
    private SQLiteUtils() {
    }

    public static String getStringFrom(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static long getLongFrom(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }

    public static double getDoubleFrom(Cursor cursor, String columnName) {
        return cursor.getDouble(cursor.getColumnIndex(columnName));
    }

    public static Date getTimestampFrom(Cursor cursor, String columnName) {
        return new Date(getLongFrom(cursor, columnName));
    }

}
