package eu.ase.bidding.utils;

import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class PermissionsUtils {

    public static void requestPermission(AppCompatActivity activity, String permissionName, int requestCode) {
        if (activity.checkSelfPermission(permissionName) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{permissionName}, requestCode);
        }
    }
}
