package eu.ase.bidding.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import eu.ase.bidding.R;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.Wallet;

public class AccountBalanceDialogFragment extends DialogFragment {
    private final Wallet wallet;
    private @Nullable
    Dialog dialog;

    public AccountBalanceDialogFragment(Wallet wallet) {
        this.wallet = wallet;
        this.dialog = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (dialog != null) {
            return dialog;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme_Black_Dialog));
        builder.setTitle(getString(R.string.account_balance));
        StringBuilder balanceStringBuilder = new StringBuilder();
        for (Currency currency : Currency.values()) {
            balanceStringBuilder.append(currency).append(": ").append(wallet.getBalance(currency)).append("\n");
        }
        builder.setMessage(balanceStringBuilder.toString());
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        return dialog;
    }
}
