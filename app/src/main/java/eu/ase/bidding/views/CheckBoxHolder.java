package eu.ase.bidding.views;

import android.widget.CheckBox;
import android.widget.CompoundButton;

public class CheckBoxHolder {
    private final CheckBox checkBox; // FIXME change to app compat
    private CompoundButton.OnCheckedChangeListener listener;

    public CheckBoxHolder(final CheckBox checkBox) {
        this.checkBox = checkBox;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void setOnCheckedChangeListener(final CompoundButton.OnCheckedChangeListener listener) {
        this.listener = listener;
        checkBox.setOnCheckedChangeListener(listener);
    }

    public void setChecked(final boolean checked, final NotifyPolicy notifyPolicy) {
        if (notifyPolicy == NotifyPolicy.NOTIFY) {
            checkBox.setChecked(checked);
        } else if (notifyPolicy == NotifyPolicy.DO_NOT_NOTIFY) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(checked);
            checkBox.setOnCheckedChangeListener(listener);
        }
    }

    @SuppressWarnings("unused")
    public void toggle(final NotifyPolicy notifyPolicy) {
        if (notifyPolicy == NotifyPolicy.NOTIFY) {
            checkBox.toggle();
        } else if (notifyPolicy == NotifyPolicy.DO_NOT_NOTIFY) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.toggle();
            checkBox.setOnCheckedChangeListener(listener);
        }
    }

    /**
     * Policy used for changing checked state
     */
    public enum NotifyPolicy {
        /**
         * Will trigger the OnCheckedChangeListener
         */
        NOTIFY,
        /**
         * Will not trigger the OnCheckedChangeListener
         */
        DO_NOT_NOTIFY
    }
}
