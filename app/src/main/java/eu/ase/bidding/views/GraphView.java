package eu.ase.bidding.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import eu.ase.bidding.models.entities.AuctionBid;

public class GraphView extends View {
    private List<AuctionBid> auctionBids;
    private Map<String, Point> pointsDataSet;
    private List<String> labelsForDataSet;
    private Paint dataPointPaint;
    private Paint dataPointFillPaint;
    private Paint dataPointLinePaint;
    private Paint axisLinePaint;
    private Paint textPaint;

    public GraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        pointsDataSet = new HashMap<>();
        labelsForDataSet = new ArrayList<>();
        auctionBids = new ArrayList<>();

        dataPointPaint = new Paint();
        dataPointPaint.setColor(Color.BLUE);
        dataPointPaint.setStrokeWidth(7f);
        dataPointPaint.setStyle(Paint.Style.STROKE);

        dataPointFillPaint = new Paint();
        dataPointFillPaint.setColor(Color.WHITE);

        dataPointLinePaint = new Paint();
        dataPointLinePaint.setColor(Color.BLUE);
        dataPointLinePaint.setStrokeWidth(7f);
        dataPointLinePaint.setAntiAlias(true);

        axisLinePaint = new Paint();
        axisLinePaint.setColor(Color.RED);
        axisLinePaint.setStrokeWidth(10f);

        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(35);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        computeDataPointsFromAuctionBids();

        final int textXOffset = 25;
        final int textYOffset = 15;

        for (int i = 0; i < labelsForDataSet.size(); i++) {
            String currentLabel = labelsForDataSet.get(i);
            Point currentDataPoint = pointsDataSet.get(currentLabel);
            int currentX = Objects.requireNonNull(currentDataPoint).x;
            int currentY = currentDataPoint.y;

            if (i < labelsForDataSet.size() - 1) {
                Point nextDataPoint = pointsDataSet.get(labelsForDataSet.get(i + 1));
                int endX = Objects.requireNonNull(nextDataPoint).x;
                int endY = nextDataPoint.y;
                canvas.drawLine(currentX, currentY, endX, endY, dataPointLinePaint);
            }

            canvas.drawCircle(currentX, currentY, 7f, dataPointFillPaint);
            canvas.drawCircle(currentX, currentY, 7f, dataPointPaint);
            canvas.drawText(currentLabel, currentX - textXOffset, currentY - textYOffset, textPaint);
        }

        canvas.drawLine(0f, 0f, 0f, getHeight(), axisLinePaint); // draw Y
        canvas.drawLine(0f, getHeight(), getWidth(), getHeight(), axisLinePaint);
    }

    public void setAuctionBids(List<AuctionBid> auctionBids) {
        this.pointsDataSet.clear();
        this.labelsForDataSet.clear();

        this.auctionBids.clear();
        this.auctionBids.addAll(auctionBids);

        invalidate();
    }

    private void computeDataPointsFromAuctionBids() {
        // see https://stackoverflow.com/questions/11483345/how-do-android-screen-coordinates-work

        if (!pointsDataSet.isEmpty()) {
            return; // already computed
        }

        double maxBid = Collections.max(auctionBids, new Comparator<AuctionBid>() {
            @Override
            public int compare(AuctionBid auctionBid1, AuctionBid auctionBid2) {
                return Double.compare(auctionBid1.getAmount(), auctionBid2.getAmount());
            }
        }).getAmount();

        int height = getHeight();
        int width = getWidth();

        int offsetFromTop = (int)(height * 0.03);
        int offsetFromLeft = (int)(width * 0.025);

        int availableWidth = getWidth() - offsetFromLeft;

        int xStep = availableWidth / auctionBids.size();

        for (AuctionBid auctionBid : auctionBids) {
            Double amount = auctionBid.getAmount();
            int offsetFromBottom = (int)((amount * height) / maxBid);
            String label = String.format(Locale.ENGLISH, "%.1f", amount);
            Point point = new Point(offsetFromLeft, height - offsetFromBottom + offsetFromTop);
            pointsDataSet.put(label, point);
            labelsForDataSet.add(label);
            offsetFromLeft += xStep;
        }
    }
}
