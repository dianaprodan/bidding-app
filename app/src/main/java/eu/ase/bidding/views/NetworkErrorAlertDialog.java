package eu.ase.bidding.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;

import eu.ase.bidding.R;

public class NetworkErrorAlertDialog {
    private Dialog dialog;

    public NetworkErrorAlertDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AppTheme_Black_Dialog));
        builder.setMessage(context.getString(R.string.network_error_message));
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.retry_later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
    }

    public void show() {
        dialog.show();
    }
}
