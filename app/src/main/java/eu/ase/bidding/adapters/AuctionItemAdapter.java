package eu.ase.bidding.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;
import eu.ase.bidding.views.CheckBoxHolder;

public class AuctionItemAdapter extends BaseAdapter implements Filterable {

    private final boolean supportFiltering;
    private List<AuctionItem> originalItems;
    private List<AuctionItem> filteredItems;
    private LayoutInflater inflater;
    private ItemFilter itemsFilter;
    private Context context;

    /**
     * Constructs adapter with configurable support filtering
     *
     * @param context          Caller context. Must be different from application context
     * @param supportFiltering Enable/Disable filtering
     */
    public AuctionItemAdapter(Context context, boolean supportFiltering) {
        this.originalItems = new ArrayList<>();
        this.filteredItems = this.originalItems; // this is needed in order to be able to use this adapter without filtering
        this.inflater = LayoutInflater.from(context);
        this.itemsFilter = new ItemFilter();
        this.context = context;
        this.supportFiltering = supportFiltering;
    }

    /**
     * Constructs adapter with disabled filtering
     *
     * @param context Caller context. Must be different from application context
     */
    public AuctionItemAdapter(Context context) {
        this(context, false);
    }

    /**
     * Retrieves the item based on his id
     *
     * @param id Id of the auction item
     * @return Auction item, if found
     * @throws RuntimeException when item was not found
     */
    public AuctionItem getById(Long id) {
        AuctionItem searchedItem = null;
        AuctionItem currentFilteredItem;
        int filteredItemsCount = getCount();
        for (int i = 0; i < filteredItemsCount; i++) {
            currentFilteredItem = getItem(i);
            if (currentFilteredItem.getId().equals(id)) {
                searchedItem = currentFilteredItem;
                break;
            }
        }
        if (searchedItem == null) {
            throw new RuntimeException("Item not found");
        }
        return searchedItem;
    }

    /**
     * Adds an item, with further filtering based on desired auction status.
     * After calling this method, publishing methods like notifyDataSetChanged does not to be called,
     * this is done internally after filtering the items.
     *
     * @param auctionItem   New item
     * @param auctionStatus Desired auction status for filtering
     */
    public void add(AuctionItem auctionItem, AuctionItemStatus.Status auctionStatus) {
        this.originalItems.add(auctionItem);
        this.getFilter().filter(auctionStatus.toString());
    }

    /**
     * Adds a list of items, without further filtering, which implies a manual call to publishing methods.
     *
     * @param items New items
     */
    public void addAll(List<AuctionItem> items) {
        this.originalItems.addAll(items);
    }

    /**
     * Adds a list of items, with further filtering based on desired auction status.
     * After calling this method, publishing methods like notifyDataSetChanged does not to be called,
     * this is done internally after filtering the items.
     *
     * @param items         New items
     * @param auctionStatus Desired auction status for filtering
     */
    public void addAll(List<AuctionItem> items, AuctionItemStatus.Status auctionStatus) {
        this.originalItems.addAll(items);
        this.getFilter().filter(auctionStatus.toString());
    }

    /**
     * Delete item by id, with further filtering based on desired auction status.
     * After calling this method, publishing methods like notifyDataSetChanged does not to be called,
     * this is done internally after filtering the items.
     *
     * @param item          Item to delete
     * @param auctionStatus Desired auction status for filtering
     */
    public void deleteById(final AuctionItem item, AuctionItemStatus.Status auctionStatus) {
        this.originalItems.removeIf(new Predicate<AuctionItem>() {
            @Override
            public boolean test(AuctionItem auctionItem) {
                return auctionItem.getId().equals(item.getId());
            }
        });
        this.getFilter().filter(auctionStatus.toString());
    }

    public List<AuctionItem> getOriginalItems() {
        return originalItems;
    }

    @Override
    public int getCount() {
        return filteredItems.size();
    }

    @Override
    public AuctionItem getItem(int position) {
        return filteredItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        // see https://stackoverflow.com/questions/24769257/custom-listview-adapter-with-filter-android

        final AuctionItem auctionItem = getItem(position);

        final ViewHolder holder;

        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.action_item, null);

            holder = new ViewHolder();
            holder.titleTextView = convertView.findViewById(R.id.auction_item_title);
            holder.descriptionTextView = convertView.findViewById(R.id.auction_item_description);
            holder.lastBidTimestampTextView = convertView.findViewById(R.id.auction_item_last_bid_timestamp);
            holder.currentPriceTextView = convertView.findViewById(R.id.auction_item_current_price);
            holder.imageImageView = convertView.findViewById(R.id.auction_item_image);
            holder.isPlacedInAuctionCheckBox = new CheckBoxHolder((CheckBox) convertView.findViewById(R.id.auction_item_place_in_auction));
            holder.soldOutButton = convertView.findViewById(R.id.auction_item_sold_out);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        Date lastBidTimestamp = auctionItem.getLastBidTimestamp();
        if (lastBidTimestamp == null) {
            lastBidTimestamp = auctionItem.getCurrentStatus().timestamp;
        }
        String lastBidTimestampString = lastBidTimestamp != null ? new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).format(lastBidTimestamp) : "";

        holder.titleTextView.setText(auctionItem.getName());
        holder.descriptionTextView.setText(auctionItem.getDescription());
        holder.lastBidTimestampTextView.setText(lastBidTimestampString);
        holder.currentPriceTextView.setText(auctionItem.currentPriceToString());
        holder.imageImageView.setImageBitmap(auctionItem.getImage());
        holder.isPlacedInAuctionCheckBox.setChecked(auctionItem.getCurrentStatus().status != AuctionItemStatus.Status.CREATED, CheckBoxHolder.NotifyPolicy.DO_NOT_NOTIFY);

        // enable if item is not placed in auction
        holder.isPlacedInAuctionCheckBox.getCheckBox().setEnabled(auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.CREATED);

        // if filtering is not supported or status different from created, auction status check box does not to be rendered
        if (!supportFiltering || auctionItem.getCurrentStatus().status != AuctionItemStatus.Status.CREATED) {
            holder.isPlacedInAuctionCheckBox.getCheckBox().setVisibility(View.GONE);
        }

        if (supportFiltering && auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.CREATED) {
            holder.isPlacedInAuctionCheckBox.getCheckBox().setVisibility(View.VISIBLE);
        }

        // hide for created items and those who don't belong to user; show for in auction
        holder.soldOutButton.setVisibility(resolveSoldOutButtonVisibility(supportFiltering, auctionItem.getCurrentStatus().status));
        // disable for items which are already sold out
        holder.soldOutButton.setEnabled(auctionItem.getCurrentStatus().status != AuctionItemStatus.Status.SOLD_OUT);
        // display sold out image
        if (supportFiltering) {
            if (auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.SOLD_OUT) {
                setSoldOutBackgroundImage(holder.soldOutButton);
            } else if (auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.IN_AUCTION) {
                setSoldOutText(holder.soldOutButton);
            }
        }

        // check box listener triggers filtering, so add it only if filtering is supported
        if (supportFiltering) {
            // overwrite listener, listener for check box of the list view item needs to be rewritten,
            // so it will contain in the closure the auction item displayed by this item
            holder.isPlacedInAuctionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.CREATED && isChecked) {
                        AuctionItemStatus nextStatus = AuctionItemStatus.nextStateFrom(auctionItem.getCurrentStatus());
                        DataRepository.persister().changeCurrentAuctionItemStatus(auctionItem, nextStatus, context, new DataPersister.OnDataPersisted<Boolean>() {
                            @Override
                            public void handle(Expect<Boolean> result) {
                                try {
                                    if (result.getResult()) {
                                        auctionItem.changeCurrentStatus();
                                        holder.isPlacedInAuctionCheckBox.getCheckBox().setEnabled(false);
                                        getFilter().filter(AuctionItemStatus.Status.CREATED.toString());
                                    }
                                } catch (Exception e) {
                                    holder.isPlacedInAuctionCheckBox.setChecked(true, CheckBoxHolder.NotifyPolicy.DO_NOT_NOTIFY);
                                    Toast.makeText(context, context.getString(R.string.place_item_in_auction_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                }
            });

            // based on the fact that filtering version is used only by user items list view
            holder.soldOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (auctionItem.getCurrentStatus().status == AuctionItemStatus.Status.IN_AUCTION) {
                        AuctionItemStatus nextStatus = AuctionItemStatus.nextStateFrom(auctionItem.getCurrentStatus());
                        DataRepository.persister().changeCurrentAuctionItemStatus(auctionItem, nextStatus, context, new DataPersister.OnDataPersisted<Boolean>() {
                            @Override
                            public void handle(Expect<Boolean> result) {
                                try {
                                    if (result.getResult()) {
                                        auctionItem.changeCurrentStatus();
                                        setSoldOutBackgroundImage(holder.soldOutButton);
                                        holder.soldOutButton.setEnabled(false);
                                    }
                                } catch (Exception e) {
                                    holder.soldOutButton.setEnabled(true);
                                    Toast.makeText(context, context.getString(R.string.sold_out_item_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                }
            });
        }

        return convertView;
    }

    private int resolveSoldOutButtonVisibility(boolean supportFiltering, AuctionItemStatus.Status currentStatus) {
        if (!supportFiltering) {
            return View.GONE;
        } else {
            return currentStatus == AuctionItemStatus.Status.CREATED ? View.GONE : View.VISIBLE;
        }
    }

    private void setSoldOutBackgroundImage(Button button) {
        button.setText(null);
        button.setBackgroundTintList(null);
        button.setBackground(context.getResources().getDrawable(R.drawable.sold_out, null));
    }

    private void setSoldOutText(Button button) {
        button.setText(context.getString(R.string.sell));
        button.setBackgroundTintList(context.getResources().getColorStateList(R.color.sell_button_color_state_list, null));
        button.setBackground(null);
    }

    @Override
    public Filter getFilter() {
        if (!supportFiltering) {
            throw new RuntimeException("Filtering is not supported");
        }
        return this.itemsFilter;
    }

    /**
     * A ViewHolder keeps references to children views to avoid unnecessary calls to findViewById() on each row.
     */
    private static class ViewHolder {
        TextView titleTextView;
        TextView descriptionTextView;
        TextView lastBidTimestampTextView;
        TextView currentPriceTextView;
        ImageView imageImageView;
        CheckBoxHolder isPlacedInAuctionCheckBox;
        Button soldOutButton;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final AuctionItemStatus.Status filterStatus = AuctionItemStatus.Status.valueOf(constraint.toString());

            FilterResults results = new FilterResults();

            final List<AuctionItem> itemList = originalItems;

            int originalItemsCount = itemList.size();
            final ArrayList<AuctionItem> filteredList = new ArrayList<>(originalItemsCount);

            AuctionItem filterableItem;

            for (int i = 0; i < originalItemsCount; i++) {
                filterableItem = itemList.get(i);
                if (needsToBeAddedToFilteredList(filterableItem.getCurrentStatus().status, filterStatus)) {
                    filteredList.add(filterableItem);
                }
            }

            results.values = filteredList;
            results.count = filteredList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredItems = (ArrayList<AuctionItem>) results.values;
            notifyDataSetChanged();
        }

        private boolean needsToBeAddedToFilteredList(AuctionItemStatus.Status itemStatus, AuctionItemStatus.Status filterStatus) {
            return filterStatus == AuctionItemStatus.Status.CREATED
                    ? itemStatus == filterStatus
                    : itemStatus != AuctionItemStatus.Status.CREATED;
        }
    }
}
