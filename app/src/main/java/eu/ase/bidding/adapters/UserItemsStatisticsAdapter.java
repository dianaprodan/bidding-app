package eu.ase.bidding.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import eu.ase.bidding.R;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.view.AuctionItemsStats;
import eu.ase.bidding.utils.StringUtils;

public class UserItemsStatisticsAdapter extends ArrayAdapter<AuctionItemsStats> {

    private LayoutInflater inflater;
    private int resource;

    public UserItemsStatisticsAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
    }

    @Override
    @NonNull
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = inflater.inflate(resource, null);
            holder = new ViewHolder();
            holder.periodTextView = convertView.findViewById(R.id.user_item_stats_period_tv);
            holder.createdTextView = convertView.findViewById(R.id.user_item_stats_created_tv);
            holder.inAuctionTextView = convertView.findViewById(R.id.user_item_stats_in_auction_tv);
            holder.soldOutTextView = convertView.findViewById(R.id.user_item_stats_sold_out_tv);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final AuctionItemsStats auctionItemStatus = getItem(position);
        assert auctionItemStatus != null;

        holder.periodTextView.setText(auctionItemStatus.period);

        AuctionItemsStats.AuctionItemStatusStats createdStats = auctionItemStatus.statusStats.get(AuctionItemStatus.Status.CREATED);
        if (createdStats != null) {
            holder.createdTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.CREATED), createdStats.count, createdStats.amount));
        } else {
            holder.createdTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.CREATED), 0, 0d));
        }

        AuctionItemsStats.AuctionItemStatusStats inAuctionStatus = auctionItemStatus.statusStats.get(AuctionItemStatus.Status.IN_AUCTION);
        if (inAuctionStatus != null) {
            holder.inAuctionTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.IN_AUCTION), inAuctionStatus.count, inAuctionStatus.amount));
        } else {
            holder.inAuctionTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.IN_AUCTION), 0, 0d));
        }

        AuctionItemsStats.AuctionItemStatusStats soldOutStatus = auctionItemStatus.statusStats.get(AuctionItemStatus.Status.SOLD_OUT);
        if (soldOutStatus != null) {
            holder.soldOutTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.SOLD_OUT), soldOutStatus.count, soldOutStatus.amount));
        } else {
            holder.soldOutTextView.setText(getContext().getString(R.string.statistics_by_status,
                    StringUtils.capitalize(AuctionItemStatus.Status.SOLD_OUT), 0, 0d));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView periodTextView;
        TextView createdTextView;
        TextView inAuctionTextView;
        TextView soldOutTextView;
    }
}
