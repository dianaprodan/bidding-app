package eu.ase.bidding.async;

import androidx.annotation.NonNull;

/**
 * Class responsible to hold the result of an async computation or it's related error.
 * It can hold only one of them at any point of time.
 *
 * @param <T> Expected result type
 */
public class Expect<T> {
    private final T result;
    private final Exception exception;

    public Expect(@NonNull final T result) {
        this.result = result;
        this.exception = null;
    }

    public Expect(@NonNull final Exception exception) {
        this.result = null;
        this.exception = exception;
    }

    /**
     * Get result of async computation.
     * If result does not exist, will throw the error of the computation.
     * Usually result should not be null.
     * However there is a case with `Void` type, which can't be instantiated, and a null is returned.
     *
     * @return Result type of the computation
     * @throws Exception Error of the computation
     */
    public T getResult() throws Exception {
        if (exception != null) {
            throw exception;
        }
        return result;
    }

    /**
     * Get error of async computation.
     * Throws if computation had no error.
     *
     * @return Error of the computation
     */
    public Exception getException() {
        if (exception == null) {
            throw new RuntimeException("No error occurred during execution of the async task");
        }
        return exception;
    }

    /**
     * Check if computation produced an error.
     */
    public boolean hasException() {
        return exception != null;
    }
}
