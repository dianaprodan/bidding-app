package eu.ase.bidding.async;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

/**
 * Generic async task. Usually a task can succeed or fail.
 * This is why it has an expected result type
 *
 * @param <Input>  Input type of the async task
 * @param <Result> Output type of the async task
 */
public class GenericTask<Input, Result> extends AsyncTask<Input, Void, Expect<Result>> {

    private Executable<Input, Result> executable;

    public GenericTask(@NonNull Executable<Input, Result> executable) {
        this.executable = executable;
    }

    /**
     * Runs in the background in a thread pol the provided executable.
     *
     * @param inputs Input parameters
     * @return Expected result of the computation
     */
    @Override
    @SafeVarargs
    protected final Expect<Result> doInBackground(Input... inputs) {
        final Input input = inputs.length > 0 ? inputs[0] : null;
        try {
            return new Expect<>(this.executable.execute(input));
        } catch (Exception e) {
            e.printStackTrace();
            return new Expect<>(e);
        }
    }

    /**
     * Runs in the main thread the provided executable with the expected result of the computation
     *
     * @param result Expected result of the computation
     */
    @Override
    protected void onPostExecute(Expect<Result> result) {
        this.executable.postExecute(result);
    }

    /**
     * Executable with can be executed by an async task
     *
     * @param <Input>  Input type of the async task
     * @param <Result> Output type of the async task
     */
    public interface Executable<Input, Result> {
        /**
         * Method which is executed in a thread different from main GUI thread.
         * Method is allowed to throw if an error occurred.
         *
         * @param input Input of the async task
         * @return Result of the computation
         */
        Result execute(Input input) throws Exception;

        /**
         * Method which is executed in the main GUI thread.
         *
         * @param result Result of the computation
         */
        void postExecute(Expect<Result> result);
    }
}
