package eu.ase.bidding.async;

import androidx.annotation.Nullable;

public class ReferenceWrapper<T> {
    public @Nullable
    T ref = null;
}
