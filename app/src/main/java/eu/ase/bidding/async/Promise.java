package eu.ase.bidding.async;

import androidx.annotation.Nullable;
import androidx.arch.core.util.Function;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Promise object represents the eventual completion (or failure)
 * of an asynchronous operation, and its resulting value.
 *
 * @param <T> Type of the expected result
 */
public class Promise<T> {
    private final Lock lock;
    private final Condition condition;
    private @Nullable Expect<T> expectedResult;

    public Promise() {
        this.lock = new ReentrantLock();
        this.condition = this.lock.newCondition();
    }

    /**
     * Resolves the promise with the result of the async computation.
     *
     * @param result Result of the computation
     */
    public void resolve(T result) {
        try {
            lock.lock();
            expectedResult = new Expect<>(result);
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Rejects the promise with the error of the async computation.
     *
     * @param error Error of the async computation
     */
    public void reject(Exception error) {
        try {
            lock.lock();
            expectedResult = new Expect<>(error);
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Awaits the result of the async computation by blocking the waiting thread.
     *
     * @return Expected result of the computation
     */
    public Expect<T> await() {
        try {
            lock.lock();
            while (expectedResult == null) {
                condition.awaitUninterruptibly();
            }
            return expectedResult;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Appends fulfillment and rejection handlers to the promise, and returns a new promise,
     * resolving to the return value of the called handler.
     *
     * @param asyncComputation  Function which performs an async computation and returns a Promise.
     *
     * @param <U>   Type of the result provided by the async computation.
     *
     * @return Promise of the provided async computation
     */
    public <U> Promise<U> then(Function<Promise<T>, Promise<U>> asyncComputation) {
        return asyncComputation.apply(this);
    }
}
