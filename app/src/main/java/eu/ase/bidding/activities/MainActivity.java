package eu.ase.bidding.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.net.HttpURLConnection;
import java.net.SocketException;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.io.network.ConnectionInterceptionManager;
import eu.ase.bidding.io.network.HttpConstants;
import eu.ase.bidding.io.network.HttpException;
import eu.ase.bidding.io.network.HttpManager;
import eu.ase.bidding.models.Wallet;
import eu.ase.bidding.repository.DataRepository;
import eu.ase.bidding.repository.DataRetriever;
import eu.ase.bidding.repository.firebase.FirebaseWalletController;
import eu.ase.bidding.repository.preferences.LocalStorage;
import eu.ase.bidding.repository.sqlite3.BiddingDatabase;
import eu.ase.bidding.utils.CryptoUtils;
import eu.ase.bidding.utils.ImageUtils;
import eu.ase.bidding.views.AccountBalanceDialogFragment;
import eu.ase.bidding.views.NetworkErrorAlertDialog;

/**
 * Main Activity responsible for set up of the application components.
 * Only it has to know about source of the data (i.e HTTP or SQLite) and interpret specific data source errors.
 * For other activities these details should be hidden using an abstract data repository.
 */
public class MainActivity extends AppCompatActivity {

    private Button _userItemsBtn;
    private Button _auctionListBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpWidgets();

        // Set up http connection interceptors
        final ConnectionInterceptionManager.Interceptor authInterceptor = new ConnectionInterceptionManager.Interceptor() {
            @Override
            public void interceptBeforeConnecting(HttpURLConnection connection) {
                try {
                    final String jwt = new LocalStorage(getApplicationContext()).get(LocalStorage.Keys.JWT_TOKEN);
                    connection.setRequestProperty(HttpConstants.HEADERS.AUTHORIZATION, "Bearer " + jwt);
                } catch (Exception e) {
                    // local storage throws an error when requested item is not found
                    e.printStackTrace();
                }
            }
        };
        HttpManager.getInstance().getConnectionInterceptionManager().add(authInterceptor);

        // Data Retriever needs to be set up before optional going to Login Activity
        DataRepository.open(getApplicationContext());

        // Set data repository result interceptor
        DataRepository.resultInterceptor = new DataRepository.Interceptor() {
            @Override
            public void postHandle(Expect<?> result, Context context) {
                if (result.hasException()) {
                    Exception exception = result.getException();
                    if (exception instanceof HttpException) {
                        HttpException httpException = (HttpException) exception;
                        if (httpException.getResponseCode() == HttpConstants.STATUS_CODES.UNAUTHORIZED ||
                                httpException.getResponseCode() == HttpConstants.STATUS_CODES.FORBIDDEN) {
                            // this navigate is safe, as expected result was not encountered by the caller,
                            // so it's clear he won't execute navigate to activities related business logic
                            navigateToLoginActivity();
                        }
                    } else if (exception instanceof SocketException) {
                        new NetworkErrorAlertDialog(context).show();
                    }
                }
            }
        };

        // check if user has a valid auth token
        DataRepository.retriever().checkToken(getApplicationContext(), new DataRetriever.OnDataRetrieved<String>() {
            @Override
            public void handle(Expect<String> data) {
                try {
                    FirebaseWalletController.getInstance().syncUserWallet(CryptoUtils.md5(data.getResult()), getApplicationContext()); // FIXME broken encapsulation
                } catch (Exception e) {
                    if (!(e instanceof SocketException)) { // let the user user use application offline
                        navigateToLoginActivity();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        MenuItem logoutMenuItem = menu.findItem(R.id.action_logout);
        logoutMenuItem.setIcon(ImageUtils.changeColor(getApplicationContext(),
                getDrawable(R.drawable.baseline_exit_to_app_black_18dp), R.color.primary_darker));

        MenuItem accountBalanceMenuItem = menu.findItem(R.id.action_account_balance);
        accountBalanceMenuItem.setIcon(ImageUtils.changeColor(getApplicationContext(),
                getDrawable(R.drawable.baseline_account_balance_wallet_black_18dp), R.color.green));

        MenuItem dropSQLiteDbMenuItem = menu.findItem(R.id.action_drop_db);
        dropSQLiteDbMenuItem.setIcon(ImageUtils.changeColor(getApplicationContext(),
                getDrawable(R.drawable.baseline_storage_black_18dp), R.color.aluminum));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                this.navigateToLoginActivity();
                break;
            case R.id.action_account_balance:
                try {
                    LocalStorage localStorage = new LocalStorage(getApplicationContext());
                    Wallet wallet = localStorage.get(LocalStorage.Keys.WALLET_OF_CURRENT_AUTHENTICATED_USER, Wallet.class);
                    AccountBalanceDialogFragment accountBalanceDialogFragment = new AccountBalanceDialogFragment(wallet);
                    accountBalanceDialogFragment.show(getSupportFragmentManager(), null);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, getString(R.string.failed_to_display_account_balance), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            case R.id.action_drop_db:
                BiddingDatabase.DeletePolicy.DROP_DB.drop();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations()) {
            DataRepository.close();
        }
    }

    private void setUpWidgets() {
        bindWidgetRefs();
        setOnClickHandlersForButtons();
    }

    private void bindWidgetRefs() {
        this._userItemsBtn = findViewById(R.id.btn_my_items);
        this._auctionListBtn = findViewById(R.id.btn_auction_list);
    }

    private void setOnClickHandlersForButtons() {
        this._userItemsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), UserItemsActivity.class);
                startActivity(intent);
            }
        });
        this._auctionListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AuctionListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void navigateToLoginActivity() {
        DataRepository.clear(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
        startActivity(intent);
    }
}
