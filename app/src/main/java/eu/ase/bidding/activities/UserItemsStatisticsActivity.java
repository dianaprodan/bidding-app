package eu.ase.bidding.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import eu.ase.bidding.R;
import eu.ase.bidding.adapters.UserItemsStatisticsAdapter;
import eu.ase.bidding.io.filesystem.TempFile;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.entities.User;
import eu.ase.bidding.models.view.AuctionItemsStats;
import eu.ase.bidding.repository.DataRepository;

public class UserItemsStatisticsActivity extends AppCompatActivity {
    private final static String STATS_JSON_KEY = "stats";

    private ListView statisticsListView;
    private UserItemsStatisticsAdapter userItemsStatisticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_items_statistics);
        bindWidgetRefs();
        setWidgetListeners();
        setWidgetAdapters();

        Optional<ArrayList<AuctionItemsStats>> auctionItemsStats = retrieveUserItemsStatistics();

        if (auctionItemsStats.isPresent()) {
            userItemsStatisticsAdapter.addAll(auctionItemsStats.get());
            userItemsStatisticsAdapter.notifyDataSetChanged();
            saveReportToJsonFile();
        } else {
            readReportFromJsonFile();
        }
    }

    private void bindWidgetRefs() {
        statisticsListView = findViewById(R.id.user_items_stats_list_view);
    }

    private void setWidgetListeners() {
        statisticsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(UserItemsStatisticsActivity.this, UserItemsChartStatisticsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM_STATS, (Serializable) parent.getItemAtPosition(position));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void setWidgetAdapters() {
        userItemsStatisticsAdapter = new UserItemsStatisticsAdapter(UserItemsStatisticsActivity.this, R.layout.auction_item_stats);
        statisticsListView.setAdapter(userItemsStatisticsAdapter);
    }

    private void saveReportToJsonFile() {
        try {
            User currentAuthenticatedUser = DataRepository.retriever().retrieveCurrentAuthenticatedUser(getApplicationContext());

            TempFile tempFile = new TempFile(getApplicationContext(), generateStatsTempFileNameForUser(currentAuthenticatedUser));
            if (!tempFile.exists()) {
                tempFile.create();
            }

            JSONObject report = new JSONObject();
            JSONArray stats = new JSONArray();
            int itemsCount = userItemsStatisticsAdapter.getCount();
            for (int i = 0; i < itemsCount; i++) {
                stats.put(Objects.requireNonNull(userItemsStatisticsAdapter.getItem(i)).toJSONObject());
            }
            report.put(STATS_JSON_KEY, stats);

            tempFile.write(report);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(UserItemsStatisticsActivity.this, getString(R.string.save_item_stats_report_failed), Toast.LENGTH_SHORT).show();
        }
    }


    private void readReportFromJsonFile() {
        try {
            User currentAuthenticatedUser = DataRepository.retriever().retrieveCurrentAuthenticatedUser(getApplicationContext());

            TempFile tempFile = new TempFile(getApplicationContext(), generateStatsTempFileNameForUser(currentAuthenticatedUser));

            JSONArray statsFromReportJSON = tempFile.readJSON().getJSONArray(STATS_JSON_KEY);
            int itemsCount = statsFromReportJSON.length();
            for (int i = 0; i < itemsCount; i++) {
                userItemsStatisticsAdapter.add(new AuctionItemsStats().fromJSONObject(statsFromReportJSON.getJSONObject(i)));
            }

            userItemsStatisticsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(UserItemsStatisticsActivity.this, getString(R.string.failed_to_compute_statistics), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("unchecked")
    private Optional<ArrayList<AuctionItemsStats>> retrieveUserItemsStatistics() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                ArrayList<ActivitiesDataPassing.AuctionItemForStatistics> itemsForStatistics =
                        (ArrayList<ActivitiesDataPassing.AuctionItemForStatistics>) bundle.getSerializable(ActivitiesDataPassing.AUCTION_ITEM_LIST_STATS);
                if (itemsForStatistics != null) {
                    final Map<String, AuctionItemsStats> auctionItemsStatsMap = new HashMap<>();

                    final SimpleDateFormat timeFormatter = new SimpleDateFormat("MM/yyyy", Locale.ENGLISH);
                    itemsForStatistics.stream().forEach(new Consumer<ActivitiesDataPassing.AuctionItemForStatistics>() {
                        @Override
                        public void accept(ActivitiesDataPassing.AuctionItemForStatistics auctionItemForStatistics) {
                            String period = timeFormatter.format(auctionItemForStatistics.currentStatus.timestamp);
                            if (auctionItemsStatsMap.containsKey(period)) {
                                AuctionItemsStats auctionItemsStats = auctionItemsStatsMap.get(period);
                                if (Objects.requireNonNull(auctionItemsStats).statusStats.containsKey(auctionItemForStatistics.currentStatus.status)) {
                                    AuctionItemsStats.AuctionItemStatusStats auctionItemStatusStats =
                                            auctionItemsStats.statusStats.get(auctionItemForStatistics.currentStatus.status);
                                    Objects.requireNonNull(auctionItemStatusStats).count += 1;
                                    Objects.requireNonNull(auctionItemStatusStats).amount += auctionItemForStatistics.currentPrice; // FIXME not taking in account currency
                                } else {
                                    AuctionItemsStats.AuctionItemStatusStats auctionItemStatusStats =
                                            new AuctionItemsStats.AuctionItemStatusStats(1, auctionItemForStatistics.currentPrice);
                                    auctionItemsStats.statusStats.put(auctionItemForStatistics.currentStatus.status, auctionItemStatusStats);
                                }
                            } else {
                                Map<AuctionItemStatus.Status, AuctionItemsStats.AuctionItemStatusStats> auctionItemStatusStatsMap =
                                        new HashMap<>();
                                AuctionItemsStats.AuctionItemStatusStats auctionItemStatusStats =
                                        new AuctionItemsStats.AuctionItemStatusStats(1, auctionItemForStatistics.currentPrice);
                                auctionItemStatusStatsMap.put(auctionItemForStatistics.currentStatus.status, auctionItemStatusStats);
                                AuctionItemsStats auctionItemsStats = new AuctionItemsStats(period, auctionItemStatusStatsMap);
                                auctionItemsStatsMap.put(period, auctionItemsStats);
                            }
                        }
                    });

                    return Optional.of(new ArrayList<>(auctionItemsStatsMap.values()));
                }
            }
        }
        return Optional.empty();
    }

    private String generateStatsTempFileNameForUser(User user) throws NoSuchAlgorithmException {
        return String.format("stats_%s.json", user.getId());
    }
}
