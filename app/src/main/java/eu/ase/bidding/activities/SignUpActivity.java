package eu.ase.bidding.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.dto.out.UserDTO;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;

public class SignUpActivity extends AppCompatActivity {

    private EditText _nameText;
    private EditText _addressText;
    private EditText _emailText;
    private EditText _mobileText;
    private EditText _passwordText;
    private EditText _reEnterPasswordText;
    private Button _signUpButton;
    private TextView _loginLink;

    private void bindWidgetsRefs() {
        this._nameText = findViewById(R.id.input_name);
        this._addressText = findViewById(R.id.input_address);
        this._emailText = findViewById(R.id.input_email);
        this._mobileText = findViewById(R.id.input_mobile);
        this._passwordText = findViewById(R.id.input_password);
        this._reEnterPasswordText = findViewById(R.id.input_reEnterPassword);
        this._signUpButton = findViewById(R.id.btn_signup);
        this._loginLink = findViewById(R.id.link_login);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        bindWidgetsRefs();

        _signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSignUp();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void doSignUp() {

        if (!validateSignUpForm()) {
            onSignUpFailed(getString(R.string.provided_info_is_not_valid_message));
            return;
        }

        _signUpButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this,
                R.style.AppTheme_Primary_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.creating_account_progress_msg));
        progressDialog.show();

        _signUpButton.setEnabled(true);

        final String name = _nameText.getText().toString().trim();
        final String address = _addressText.getText().toString().trim();
        final String email = _emailText.getText().toString().trim();
        final String mobile = _mobileText.getText().toString().trim();
        final String password = _passwordText.getText().toString().trim();
        final UserDTO user = new UserDTO(email, password, name, address, email, mobile);

        DataRepository.persister().registerUser(user, SignUpActivity.this, new DataPersister.OnDataPersisted<Boolean>() {
            @Override
            public void handle(Expect<Boolean> result) {
                try {
                    progressDialog.dismiss();
                    if (result.getResult()) { // might throw if something went wrong
                        finish();
                    }
                } catch (Exception e) {
                    onSignUpFailed(getString(R.string.user_registering_failed));
                }
            }
        });
    }

    private void onSignUpFailed(String errorMessage) {
        Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_LONG).show();

        _signUpButton.setEnabled(true);
    }

    private boolean validateSignUpForm() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError(getString(R.string.invalid_name_error_msg));
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (address.isEmpty()) {
            _addressText.setError(getString(R.string.invalid_address_error_msg));
            valid = false;
        } else {
            _addressText.setError(null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.invalid_email_error_msg));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (mobile.length() != 10) {
            _mobileText.setError(getString(R.string.invalid_mobile_number_error_msg));
            valid = false;
        } else {
            _mobileText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError(getString(R.string.invalid_password_error_msg));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (!reEnterPassword.equals(password)) {
            _reEnterPasswordText.setError(getString(R.string.invalid_password_miss_match_error_msg));
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }
}