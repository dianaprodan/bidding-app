package eu.ase.bidding.activities;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

import eu.ase.bidding.R;
import eu.ase.bidding.models.Coordinate;
import eu.ase.bidding.utils.ImageUtils;

public class AuctionItemLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_item_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Coordinate itemCoordinate = (Coordinate) Objects.requireNonNull(getIntent().getExtras())
                .getSerializable(ActivitiesDataPassing.AUCTION_ITEM_COORDINATES);

        // Add a marker in Sydney and move the camera
        LatLng itemLocation = new LatLng(Objects.requireNonNull(itemCoordinate).latitude, itemCoordinate.longitude);
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(itemLocation)
                .icon(ImageUtils.bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_home_black_24dp)));
        marker.showInfoWindow();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(itemLocation, 15));
    }
}
