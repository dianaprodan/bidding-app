package eu.ase.bidding.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import java.util.ArrayList;
import java.util.List;

import eu.ase.bidding.R;
import eu.ase.bidding.adapters.AuctionItemAdapter;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;
import eu.ase.bidding.repository.DataRetriever;
import eu.ase.bidding.utils.ImageUtils;

public class UserItemsActivity extends AppCompatActivity {
    private static final Integer REQUEST_ADD_ITEM = 0;
    private static final Integer REQUEST_DISPLAY_STATISTICS = 1;

    private ListView listViewUserAuctionItems;
    private SwitchCompat inAuctionListSwitch;
    private AuctionItemAdapter userAuctionItemsAdapter;

    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_items);

        bindWidgetRefs();

        this.userAuctionItemsAdapter = new AuctionItemAdapter(UserItemsActivity.this, true);
        this.listViewUserAuctionItems.setAdapter(this.userAuctionItemsAdapter);

        setUpWidgetListeners();

        final ProgressDialog progressDialog = new ProgressDialog(UserItemsActivity.this, R.style.AppTheme_Primary_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.retrieving_auction_items_progress_msg));
        progressDialog.show();

        DataRepository.retriever().retrieveAuctionItems(AuctionItem.Owner.USER, UserItemsActivity.this, new DataRetriever.OnDataRetrieved<List<AuctionItem>>() {
            @Override
            public void handle(Expect<List<AuctionItem>> data) {
                try {
                    progressDialog.dismiss();

                    List<AuctionItem> items = data.getResult();
                    if (items.isEmpty()) {
                        Toast.makeText(getApplicationContext(), getString(R.string.items_were_not_found_msg), Toast.LENGTH_LONG).show();
                        return;
                    }

                    userAuctionItemsAdapter.addAll(items, getGlobalAuctionStatusFilter());
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.retrieving_auction_list_failed_msg), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setUpWidgetListeners() {
        this.inAuctionListSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean inAuctionList) {
                userAuctionItemsAdapter.getFilter().filter(getGlobalAuctionStatusFilter().toString());
            }
        });

        this.listViewUserAuctionItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(UserItemsActivity.this, R.style.AppTheme_Black_Dialog))
                        .setMessage(getString(R.string.ask_for_user_item_deletion))
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.positive_answer), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                final AuctionItem auctionItem = (AuctionItem) parent.getItemAtPosition(position);

                                DataRepository.persister().deleteAuctionItem(auctionItem.getId(), UserItemsActivity.this, new DataPersister.OnDataPersisted<Boolean>() {
                                    @Override
                                    public void handle(Expect<Boolean> result) {
                                        try {
                                            dialog.dismiss();

                                            if (result.getResult()) {
                                                userAuctionItemsAdapter.deleteById(auctionItem, getGlobalAuctionStatusFilter());
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(getApplicationContext(), getString(R.string.item_deletion_failed_msg), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton(getString(R.string.negative_answer), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create();

                // see https://stackoverflow.com/questions/30743038/align-alertdialog-buttons-to-center
                dialog.show();

                final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                positiveButtonLL.gravity = Gravity.CENTER_HORIZONTAL;
                positiveButton.setTextColor(getColor(R.color.primary_darker));
                positiveButton.setLayoutParams(positiveButtonLL);

                return false;
            }
        });
    }

    private void bindWidgetRefs() {
        this.listViewUserAuctionItems = findViewById(R.id.listViewUserAuctionItems);
        this.inAuctionListSwitch = findViewById(R.id.switchInAuctionItems);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_my_items, menu);

        MenuItem addUserItemMenuItem = menu.findItem(R.id.action_add_user_item);
        addUserItemMenuItem.setIcon(ImageUtils.changeColor(getApplicationContext(),
                getDrawable(R.drawable.baseline_add_shopping_cart_black_18dp), R.color.primary_darker));

        MenuItem userItemStatsMenuItem = menu.findItem(R.id.action_user_item_statistics);
        userItemStatsMenuItem.setIcon(ImageUtils.changeColor(getApplicationContext(),
                getDrawable(R.drawable.baseline_pie_chart_black_18dp), R.color.aluminum));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_add_user_item:
                intent = new Intent(getApplicationContext(), AddItemActivity.class);
                startActivityForResult(intent, REQUEST_ADD_ITEM);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.action_user_item_statistics:
                intent = new Intent(getApplicationContext(), UserItemsStatisticsActivity.class);

                List<AuctionItem> auctionItems = userAuctionItemsAdapter.getOriginalItems();
                int numberOfItems = auctionItems.size();

                if (numberOfItems > 0) {
                    Bundle bundle = new Bundle();
                    ArrayList<ActivitiesDataPassing.AuctionItemForStatistics> auctionItemForStatistics = new ArrayList<>();
                    for (int i = 0; i < numberOfItems; i++) {
                        AuctionItem auctionItem = auctionItems.get(i);
                        auctionItemForStatistics.add(new ActivitiesDataPassing.AuctionItemForStatistics(
                                auctionItem.getCurrentStatus(), auctionItem.getCurrentPrice(), auctionItem.getCurrency()
                        ));
                    }
                    bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM_LIST_STATS, auctionItemForStatistics);
                    intent.putExtras(bundle);
                }

                startActivityForResult(intent, REQUEST_DISPLAY_STATISTICS);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ADD_ITEM && resultCode == RESULT_OK && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle == null) {
                return;
            }

            AuctionItem createdItem = (AuctionItem) bundle.getSerializable(ActivitiesDataPassing.AUCTION_ITEM);
            this.userAuctionItemsAdapter.add(createdItem, getGlobalAuctionStatusFilter());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public AuctionItemStatus.Status getGlobalAuctionStatusFilter() {
        return inAuctionListSwitch.isChecked() ? AuctionItemStatus.Status.IN_AUCTION : AuctionItemStatus.Status.CREATED;
    }
}
