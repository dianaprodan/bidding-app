package eu.ase.bidding.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.dto.out.AuthenticationRequest;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;

public class LogInActivity extends AppCompatActivity {
    private EditText _emailText;
    private EditText _passwordText;
    private Button _loginButton;
    private TextView _signUpLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bindWidgetsRefs();

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signUpLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private void bindWidgetsRefs() {
        this._emailText = findViewById(R.id.input_email);
        this._passwordText = findViewById(R.id.input_password);
        this._loginButton = findViewById(R.id.btn_login);
        this._signUpLink = findViewById(R.id.link_signup);
    }

    private void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final String email = _emailText.getText().toString().trim();
        final String password = _passwordText.getText().toString().trim();

        DataRepository.persister().authenticateUser(new AuthenticationRequest(email, password), LogInActivity.this, new DataPersister.OnDataPersisted<Boolean>() {
            @Override
            public void handle(Expect<Boolean> result) {
                try {
                    Boolean authenticationStatus = result.getResult();
                    if (authenticationStatus) {
                        onLoginSuccess();
                    } else {
                        onLoginFailed();
                    }
                } catch (Exception e) {
                    onLoginFailed();
                }
            }
        });
    }

    private void onLoginSuccess() {
        _loginButton.setEnabled(true);
        finish();
    }

    private void onLoginFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.authentication_failed_msg), Toast.LENGTH_LONG).show();
        _loginButton.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.invalid_email_error_msg));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError(getString(R.string.invalid_password_error_msg));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}
