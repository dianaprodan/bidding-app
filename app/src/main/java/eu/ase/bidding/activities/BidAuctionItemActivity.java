package eu.ase.bidding.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.entities.AuctionBid;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;

public class BidAuctionItemActivity extends AppCompatActivity {

    private static final int MAX_NUMBER_OF_RATING_STARS = 5;
    private static final int BID_PRICE_SEEK_BAR_INCREMENT = 100;
    private static final int BID_PRICE_SEEK_BAR_MAX_OFFSET_FROM_CURRENT_PRICE = 1000;

    private ImageView itemImageView;
    private TextView itemNameTextView;
    private TextView itemOwnerTextView;
    private TextView itemDescriptionTextView;
    private TextView itemCurrentPriceTextView;
    private RatingBar itemRatingBar;
    private EditText itemBidPriceEditText;
    private TextView itemBidCurrencyTextView;
    private SeekBar itemBidPriceSeekBar;
    private Button itemBidButton;
    private Button showBidsGraphButton;
    private Button showLocationButton;

    private AuctionItem displayedAuctionItem;
    private @Nullable
    Float selectedRatingStars = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_auction_item);
        bindWidgetsRefs();
        setUpWidgets();
        populateWidgetsIfReceivedBundle().ifPresent(new Consumer<AuctionItem>() {
            @Override
            public void accept(AuctionItem item) {
                displayedAuctionItem = item;
            }
        });
        setWidgetsHandlers();
    }

    private void bindWidgetsRefs() {
        this.itemImageView = findViewById(R.id.auction_item_bid_image);
        this.itemNameTextView = findViewById(R.id.auction_item_bid_name);
        this.itemOwnerTextView = findViewById(R.id.auction_item_bid_owner);
        this.itemDescriptionTextView = findViewById(R.id.auction_item_bid_description);
        this.itemCurrentPriceTextView = findViewById(R.id.auction_item_bid_current_price);
        this.itemRatingBar = findViewById(R.id.auction_item_bid_rating);
        this.itemBidPriceEditText = findViewById(R.id.auction_item_bid_price_edit_text);
        this.itemBidCurrencyTextView = findViewById(R.id.auction_item_bid_price_currency);
        this.itemBidPriceSeekBar = findViewById(R.id.auction_item_bid_price_seek_bar);
        this.itemBidButton = findViewById(R.id.auction_item_bid_make_bid_button);
        this.showBidsGraphButton = findViewById(R.id.auction_item_show_bid_graph_button);
        this.showLocationButton = findViewById(R.id.auction_item_show_location_button);
    }

    private void setUpWidgets() {
        this.itemRatingBar.setMax(MAX_NUMBER_OF_RATING_STARS);
    }

    private Optional<AuctionItem> populateWidgetsIfReceivedBundle() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                AuctionItem item = (AuctionItem) bundle.getSerializable(ActivitiesDataPassing.AUCTION_ITEM);
                this.itemImageView.setImageBitmap(Objects.requireNonNull(item).getImage());
                this.itemNameTextView.setText(item.getName());
                item.getOwner().intoTextView(this.itemOwnerTextView);
                this.itemDescriptionTextView.setText(item.getDescription());
                this.itemCurrentPriceTextView.setText(String.format("%s %s", item.getCurrentPrice(), item.getCurrency()));
                this.itemRatingBar.setRating(item.getRating());
                this.itemBidPriceEditText.setText(String.format(Locale.ENGLISH, "%.2f", item.getCurrentPrice()));
                this.itemBidCurrencyTextView.setText(item.getCurrency().toString());
                this.itemBidPriceSeekBar.setMax(BID_PRICE_SEEK_BAR_MAX_OFFSET_FROM_CURRENT_PRICE);
                this.itemBidPriceSeekBar.setKeyProgressIncrement(BID_PRICE_SEEK_BAR_INCREMENT);
                return Optional.of(item);
            }
        }
        return Optional.empty();
    }

    private void setWidgetsHandlers() {
        this.itemBidPriceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double newPrice = displayedAuctionItem.getCurrentPrice() + progress;
                itemBidPriceEditText.setText(String.format(Locale.ENGLISH, "%.2f", newPrice));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        this.itemRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (fromUser) {
                    DataRepository.persister().persistRatingStars(displayedAuctionItem, rating, BidAuctionItemActivity.this, new DataPersister.OnDataPersisted<Float>() {
                        @Override
                        public void handle(Expect<Float> result) {
                            try {
                                selectedRatingStars = result.getResult();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getString(R.string.adding_rating_stars_failed), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        this.itemBidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFormValid()) {
                    return;
                }

                Double bidAmount = Double.parseDouble(itemBidPriceEditText.getText().toString());

                DataRepository.persister().persistBid(displayedAuctionItem.getId(), bidAmount, displayedAuctionItem.getCurrency(), BidAuctionItemActivity.this, new DataPersister.OnDataPersisted<AuctionBid>() {
                    @Override
                    public void handle(Expect<AuctionBid> result) {
                        try {
                            AuctionBid auctionBid = result.getResult();
                            ActivitiesDataPassing.AuctionItemAfterBiddingChanges auctionItemAfterBiddingChangesChanges =
                                    new ActivitiesDataPassing.AuctionItemAfterBiddingChanges(displayedAuctionItem.getId(), selectedRatingStars, auctionBid);

                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM, auctionItemAfterBiddingChangesChanges);
                            intent.putExtras(bundle);

                            setResult(RESULT_OK, intent);
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), getString(R.string.adding_bid_failed), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        this.showBidsGraphButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<AuctionBid> auctionBids = displayedAuctionItem.getBids();
                if (auctionBids.size() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_bids), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent displayBidsIntent = new Intent(BidAuctionItemActivity.this, BidsGraphActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM_BIDS, (ArrayList) auctionBids);
                displayBidsIntent.putExtras(bundle);

                startActivity(displayBidsIntent);
            }
        });

        this.showLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent displayBidsIntent = new Intent(BidAuctionItemActivity.this, AuctionItemLocationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM_COORDINATES, displayedAuctionItem.getCoordinate());
                displayBidsIntent.putExtras(bundle);

                startActivity(displayBidsIntent);
            }
        });
    }

    private boolean isFormValid() {
        boolean isValid = true;

        String price = this.itemBidPriceEditText.getText().toString();

        if (price.isEmpty()) {
            this.itemBidPriceEditText.setError("Enter valid price");
            isValid = false;
        } else if (Double.parseDouble(price) <= this.displayedAuctionItem.getCurrentPrice()) {
            this.itemBidPriceEditText.setError("Enter higher price than current");
            isValid = false;
        } else {
            this.itemBidPriceEditText.setError(null);
        }

        return isValid;
    }
}
