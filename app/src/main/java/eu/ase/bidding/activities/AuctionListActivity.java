package eu.ase.bidding.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.Objects;

import eu.ase.bidding.R;
import eu.ase.bidding.adapters.AuctionItemAdapter;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.DataRepository;
import eu.ase.bidding.repository.DataRetriever;

public class AuctionListActivity extends AppCompatActivity {
    private static final int BID_FOR_AUCTION_ITEM_REQUEST_CODE = 0;
    private ListView listViewAuctionList;
    private AuctionItemAdapter auctionItemsAdapter;

    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_list);
        bindWidgetRefs();
        setOnClickListeners();

        this.auctionItemsAdapter = new AuctionItemAdapter(AuctionListActivity.this);

        this.listViewAuctionList.setAdapter(this.auctionItemsAdapter);

        final ProgressDialog progressDialog = new ProgressDialog(AuctionListActivity.this,
                R.style.AppTheme_Primary_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.retrieving_auction_items_progress_msg));
        progressDialog.show();

        DataRepository.retriever().retrieveAuctionItems(AuctionItem.Owner.OTHERS, AuctionListActivity.this, new DataRetriever.OnDataRetrieved<List<AuctionItem>>() {
            @Override
            public void handle(Expect<List<AuctionItem>> data) {
                try {
                    progressDialog.dismiss();

                    List<AuctionItem> items = data.getResult();
                    if (items.isEmpty()) {
                        Toast.makeText(getApplicationContext(), getString(R.string.items_were_not_found_msg), Toast.LENGTH_LONG).show();
                        return;
                    }

                    auctionItemsAdapter.addAll(items);
                    auctionItemsAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.retrieving_auction_list_failed_msg), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == BID_FOR_AUCTION_ITEM_REQUEST_CODE && data != null) {
            Bundle bundle = Objects.requireNonNull(data.getExtras());
            final ActivitiesDataPassing.AuctionItemAfterBiddingChanges updatedItem =
                    (ActivitiesDataPassing.AuctionItemAfterBiddingChanges) bundle.getSerializable(ActivitiesDataPassing.AUCTION_ITEM);

            final AuctionItem itemFromAdapter = this.auctionItemsAdapter.getById(Objects.requireNonNull(updatedItem).id);
            itemFromAdapter.addBid(updatedItem.bid);

            if (updatedItem.stars != null) {
                itemFromAdapter.addStars(updatedItem.stars);
            }

            this.auctionItemsAdapter.notifyDataSetChanged();
        }
    }

    private void bindWidgetRefs() {
        this.listViewAuctionList = findViewById(R.id.auction_list_list_view);
    }

    private void setOnClickListeners() {
        this.listViewAuctionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final AuctionItem item = (AuctionItem) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), BidAuctionItemActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM, item);
                intent.putExtras(bundle);

                startActivityForResult(intent, BID_FOR_AUCTION_ITEM_REQUEST_CODE);
            }
        });
    }
}
