package eu.ase.bidding.activities;

import androidx.annotation.Nullable;

import java.io.Serializable;

import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.entities.AuctionBid;

class ActivitiesDataPassing {
    static final String AUCTION_ITEM = "AUCTION_ITEM";
    static final String AUCTION_ITEM_LIST_STATS = "AUCTION_ITEM_LIST_STATS";
    static final String AUCTION_ITEM_STATS = "AUCTION_ITEM_STATS";
    static final String AUCTION_ITEM_BIDS = "AUCTION_ITEM_BIDS";
    static final String AUCTION_ITEM_COORDINATES = "AUCTION_ITEM_COORDINATES";

    private ActivitiesDataPassing() {
    }

    public static class AuctionItemAfterBiddingChanges implements Serializable {
        public Long id;
        @Nullable
        Float stars;
        AuctionBid bid;

        AuctionItemAfterBiddingChanges(Long id, @Nullable Float stars, AuctionBid bid) {
            this.id = id;
            this.stars = stars;
            this.bid = bid;
        }
    }

    static class AuctionItemForStatistics implements Serializable {
        final AuctionItemStatus currentStatus;
        final Double currentPrice;
        final Currency currentPriceCurrency;

        AuctionItemForStatistics(AuctionItemStatus currentStatus, Double currentPrice, Currency currentPriceCurrency) {
            this.currentStatus = currentStatus;
            this.currentPrice = currentPrice;
            this.currentPriceCurrency = currentPriceCurrency;
        }
    }
}
