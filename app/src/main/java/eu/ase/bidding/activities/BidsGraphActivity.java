package eu.ase.bidding.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import eu.ase.bidding.R;
import eu.ase.bidding.models.entities.AuctionBid;
import eu.ase.bidding.views.GraphView;

public class BidsGraphActivity extends AppCompatActivity {

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bids_graph);

        GraphView bidsGraphView = findViewById(R.id.bids_graph_view);
        bidsGraphView.setAuctionBids((ArrayList<AuctionBid>)
                Objects.requireNonNull(getIntent().getExtras()).getSerializable(ActivitiesDataPassing.AUCTION_ITEM_BIDS));
    }
}
