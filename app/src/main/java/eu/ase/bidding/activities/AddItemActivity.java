package eu.ase.bidding.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.Objects;

import eu.ase.bidding.R;
import eu.ase.bidding.async.Expect;
import eu.ase.bidding.models.Coordinate;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.dto.out.AuctionItemDTO;
import eu.ase.bidding.models.entities.AuctionItem;
import eu.ase.bidding.repository.DataPersister;
import eu.ase.bidding.repository.DataRepository;
import eu.ase.bidding.utils.EnumUtils;
import eu.ase.bidding.utils.ImageUtils;
import eu.ase.bidding.utils.PermissionsUtils;

public class AddItemActivity extends AppCompatActivity {

    private final static int GALLERY_REQUEST_CODE = 0;
    private final static int READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1;
    private final static int ACCESS_FINE_LOCATION_PERMISSION_REQUEST_CODE = 2;
    private final static int MAX_IMAGE_SIZE_BYTES = 307200; // 300 KB
    private final static int MAX_IMAGE_HEIGHT = 1000;
    private final static int MAX_IMAGE_WIDTH = 1000;

    private EditText _nameText;
    private EditText _descriptionText;
    private EditText _startingPriceValueText;
    private Spinner _startingPriceCurrencySpinner;
    private Button _selectImageButton;
    private Button _createItemButton;
    private Button _cancelButton;
    private Bitmap _itemImage;
    private String _selectedCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_item);
        bindWidgetRefs();
        setUpStartingPriceCurrencySpinner();
        setUpOnButtonClickHandlers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            if (selectedImage == null) {
                return;
            }

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null) {
                return;
            }

            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();

            final File imageFile = new File(imagePath);
            if (imageFile.length() > MAX_IMAGE_SIZE_BYTES) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.select_item_image_too_big_error_msg) + MAX_IMAGE_SIZE_BYTES + "BYTES",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            this._itemImage = BitmapFactory.decodeFile(imagePath);
            if (this._itemImage == null) {
                PermissionsUtils.requestPermission(this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
            } else if (this._itemImage.getWidth() > MAX_IMAGE_WIDTH || this._itemImage.getHeight() > MAX_IMAGE_HEIGHT) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.select_item_image_too_high_resolution) + MAX_IMAGE_HEIGHT + "x" + MAX_IMAGE_WIDTH,
                        Toast.LENGTH_SHORT).show();
                this._itemImage = null;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(getApplicationContext(), getString(R.string.request_for_external_storage_perms), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == ACCESS_FINE_LOCATION_PERMISSION_REQUEST_CODE) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(getApplicationContext(), getString(R.string.request_for_location_perms), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void bindWidgetRefs() {
        this._nameText = findViewById(R.id.input_item_name);
        this._descriptionText = findViewById(R.id.input_item_description);
        this._startingPriceValueText = findViewById(R.id.input_item_starting_price_value);
        this._startingPriceCurrencySpinner = findViewById(R.id.spinner_item_starting_price_currency);
        this._selectImageButton = findViewById(R.id.btn_item_image);
        this._createItemButton = findViewById(R.id.btn_create_user_item);
        this._cancelButton = findViewById(R.id.btn_cancel_user_item_creation);
    }

    private void setUpStartingPriceCurrencySpinner() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_item, EnumUtils.getNames(Currency.class));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this._startingPriceCurrencySpinner.setAdapter(arrayAdapter);
        this._startingPriceCurrencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _selectedCurrency = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void setUpOnButtonClickHandlers() {
        this._selectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/png", "image/jpg"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        this._createItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateAddUserItemForm()) {
                    return;
                }

                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtils.requestPermission(AddItemActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_FINE_LOCATION_PERMISSION_REQUEST_CODE);
                    return;
                }

                _createItemButton.setEnabled(false);

                final ProgressDialog progressDialog = new ProgressDialog(AddItemActivity.this, R.style.AppTheme_Primary_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage(getString(R.string.creating_user_item_progress_msg));
                progressDialog.show();

                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Objects.requireNonNull(locationManager).requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        final AuctionItemDTO auctionItemDTO = new AuctionItemDTO(_nameText.getText().toString().trim(),
                                _descriptionText.getText().toString().trim(), "", Currency.valueOf(_selectedCurrency),
                                Double.parseDouble(_startingPriceValueText.getText().toString()),
                                ImageUtils.convert(_itemImage), new Coordinate(location.getLatitude(), location.getLongitude()));

                        DataRepository.persister().createAuctionItem(auctionItemDTO, AddItemActivity.this, new DataPersister.OnDataPersisted<AuctionItem>() {
                            @Override
                            public void handle(Expect<AuctionItem> result) {
                                try {
                                    progressDialog.dismiss();

                                    final AuctionItem persistedAuctionItem = result.getResult(); // will throw with occurred exception, if no result exists

                                    Intent intent = new Intent();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable(ActivitiesDataPassing.AUCTION_ITEM, persistedAuctionItem);
                                    intent.putExtras(bundle);

                                    setResult(RESULT_OK, intent);
                                    finish();
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.creating_auction_item_failed), Toast.LENGTH_LONG).show();
                                    _createItemButton.setEnabled(true);
                                }
                            }
                        });
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }
                }, null);
            }
        });

        this._cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private boolean validateAddUserItemForm() {
        boolean isValid = true;

        String name = this._nameText.getText().toString();
        String description = this._descriptionText.getText().toString();
        String startPriceValue = this._startingPriceValueText.getText().toString();

        if (AuctionItem.Validator.validateName(name)) {
            this._nameText.setError(null);
        } else {
            this._nameText.setError(getString(R.string.invalid_item_name_error_msg));
            isValid = false;
        }

        if (AuctionItem.Validator.validateDescription(description)) {
            this._descriptionText.setError(null);
        } else {
            this._descriptionText.setError(getString(R.string.invalid_item_description_error_msg));
            isValid = false;
        }

        if (!startPriceValue.equals("") &&
                AuctionItem.Validator.validateStartingPrice(Double.parseDouble(startPriceValue))) {
            this._startingPriceValueText.setError(null);
        } else {
            this._startingPriceValueText.setError(getString(R.string.invalid_item_starting_price_error_msg));
            isValid = false;
        }

        if (this._selectedCurrency == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.item_currency_not_selected_error_msg), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (this._itemImage == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.item_image_not_selected_error_msg), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }
}
