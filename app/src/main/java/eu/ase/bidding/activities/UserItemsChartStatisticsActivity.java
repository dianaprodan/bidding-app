package eu.ase.bidding.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.ase.bidding.R;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.view.AuctionItemsStats;
import eu.ase.bidding.utils.StringUtils;

public class UserItemsChartStatisticsActivity extends AppCompatActivity {

    private AuctionItemsStats auctionItemsStats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_items_chart_statistics);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                auctionItemsStats = (AuctionItemsStats) bundle.getSerializable(ActivitiesDataPassing.AUCTION_ITEM_STATS);
            }
        }

        PieChart userItemsPieChart = findViewById(R.id.user_items_stats_pie_chart);
        userItemsPieChart.setUsePercentValues(true);

        List<PieEntry> yValues = new ArrayList<>();
        for (Map.Entry<AuctionItemStatus.Status, AuctionItemsStats.AuctionItemStatusStats> statusStats : auctionItemsStats.statusStats.entrySet()) {
            yValues.add(new PieEntry(statusStats.getValue().amount.floatValue(), StringUtils.capitalize(statusStats.getKey())));
        }

        PieDataSet dataSet = new PieDataSet(yValues, null);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(13f);

        Description description = new Description();
        description.setText(getString(R.string.items_stats_by_status));

        userItemsPieChart.getLegend().setWordWrapEnabled(true);
        userItemsPieChart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        userItemsPieChart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        userItemsPieChart.getLegend().setFormSize(20f);
        userItemsPieChart.getLegend().setFormToTextSpace(5f);
        userItemsPieChart.getLegend().setForm(Legend.LegendForm.SQUARE);
        userItemsPieChart.getLegend().setTextSize(12f);
        userItemsPieChart.getLegend().setOrientation(Legend.LegendOrientation.HORIZONTAL);
        userItemsPieChart.getLegend().setDrawInside(false);

        userItemsPieChart.setData(data);
        userItemsPieChart.setDescription(description);
        userItemsPieChart.setDrawEntryLabels(false);
        userItemsPieChart.setExtraOffsets(20f, 0f, 20f, 0f);
        userItemsPieChart.setUsePercentValues(true);
        userItemsPieChart.setDrawHoleEnabled(true);
        userItemsPieChart.setCenterText(auctionItemsStats.period);
        userItemsPieChart.setCenterTextSize(10f);
        userItemsPieChart.setCenterTextTypeface(Typeface.SANS_SERIF);
        userItemsPieChart.setRotationEnabled(false);
        userItemsPieChart.animateXY(5000, 5000);
    }
}
