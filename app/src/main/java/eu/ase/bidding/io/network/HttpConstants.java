package eu.ase.bidding.io.network;

public class HttpConstants {
    private HttpConstants() {
    }

    static class METHODS {
        static final String GET = "GET";
        static final String POST = "POST";
        static final String PUT = "PUT";
        static final String DELETE = "DELETE";
    }

    public static class HEADERS {
        public static final String AUTHORIZATION = "Authorization";
        static final String ACCEPT_LANGUAGE = "Accept-Language";
        static final String CONTENT_TYPE = "Content-Type";
        static final String CONTENT_LENGTH = "Content-Length";
        static final String TRANSFER_ENCODING = "Transfer-Encoding";
    }

    public static class STATUS_CODES {
        public static final int UNAUTHORIZED = 401;
        public static final int FORBIDDEN = 403;
        static final int OK = 200;
    }
}
