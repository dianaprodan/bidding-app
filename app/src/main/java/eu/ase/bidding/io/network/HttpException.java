package eu.ase.bidding.io.network;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.util.Optional;

public class HttpException extends RuntimeException {
    private final int responseCode;
    private final JSONObject jsonBody;

    public HttpException(int responseCode, @Nullable JSONObject jsonBody) {
        this.responseCode = responseCode;
        this.jsonBody = jsonBody;
    }

    public Optional<JSONObject> getJSONBody() {
        return Optional.ofNullable(jsonBody);
    }

    public int getResponseCode() {
        return responseCode;
    }
}
