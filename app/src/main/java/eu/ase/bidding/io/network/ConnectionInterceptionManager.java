package eu.ase.bidding.io.network;

import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;

public class ConnectionInterceptionManager {

    private List<Interceptor> interceptors = new LinkedList<>();

    public void add(Interceptor interceptor) {
        interceptors.add(interceptor);
    }

    void executeAllBeforeConnectingInterceptors(HttpURLConnection connection) {
        for (Interceptor interceptor : interceptors) {
            interceptor.interceptBeforeConnecting(connection);
        }
    }

    public abstract static class Interceptor {
        public abstract void interceptBeforeConnecting(HttpURLConnection connection);
    }
}
