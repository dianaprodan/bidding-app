package eu.ase.bidding.io.filesystem;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;

import eu.ase.bidding.persistence.Xmlable;
import eu.ase.bidding.utils.FileUtils;

public class TempFile {
    private final Context appContext;
    private File cacheFile;

    public TempFile(Context appContext, String fileName) {
        this.appContext = appContext;
        this.cacheFile = new File(appContext.getCacheDir(), fileName);
    }

    public boolean exists() {
        return this.cacheFile.exists();
    }

    public TempFile create() throws IOException {
        if (!this.cacheFile.createNewFile()) {
            throw new RuntimeException("Failed to create file");
        }
        return this;
    }

    public void write(JSONObject json) throws IOException {
        if (FileUtils.deviceHasNoAvailableSpace(appContext)) {
            throw new RuntimeException("No available space");
        }
        FileUtils.write(cacheFile, json.toString().getBytes(), appContext);
    }

    public boolean write(Xmlable xmlable) throws IOException {
        if (FileUtils.deviceHasNoAvailableSpace(appContext)) {
            return false;
        }
        FileUtils.write(cacheFile, xmlable);
        return true;
    }

    public JSONObject readJSON() throws IOException, JSONException {
        return new JSONObject(FileUtils.read(cacheFile, appContext));
    }

    public <T> T readXML(Class<T> xmlConstructable) throws Exception {
        return xmlConstructable.getConstructor(Document.class).newInstance(FileUtils.readXML(cacheFile));
    }

    public boolean delete() {
        return cacheFile.delete();
    }
}
