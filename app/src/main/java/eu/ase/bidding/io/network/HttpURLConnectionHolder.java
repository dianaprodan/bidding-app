package eu.ase.bidding.io.network;

import java.net.HttpURLConnection;

public class HttpURLConnectionHolder implements AutoCloseable {
    private final HttpURLConnection connection;

    HttpURLConnectionHolder(final HttpURLConnection connection) {
        this.connection = connection;
    }

    HttpURLConnection getConnection() {
        return connection;
    }

    @Override
    public void close() {
        connection.disconnect();
    }
}
