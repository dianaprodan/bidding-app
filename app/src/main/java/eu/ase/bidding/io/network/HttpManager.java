package eu.ase.bidding.io.network;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Optional;

import eu.ase.bidding.persistence.Jsonable;

public class HttpManager {
    private static HttpManager instance = null;
    private ConnectionInterceptionManager connectionInterceptionManager;


    private HttpManager() {
        connectionInterceptionManager = new ConnectionInterceptionManager();
    }

    public static synchronized HttpManager getInstance() {
        if (instance == null) {
            instance = new HttpManager();
        }
        return instance;
    }

    public ConnectionInterceptionManager getConnectionInterceptionManager() {
        return connectionInterceptionManager;
    }

    private String appendQueryParams(String urlWithoutQueryParams, Map<String, String> queryParams) throws UnsupportedEncodingException {
        StringBuilder urlBuilder = new StringBuilder(urlWithoutQueryParams);
        urlBuilder.append("?");
        String separator = "";
        for (Map.Entry<String, String> param : queryParams.entrySet()) {
            urlBuilder.append(separator);
            separator = "&";
            urlBuilder.append(param.getKey()).append("=").append(URLEncoder.encode(param.getValue(), "UTF-8"));
        }
        return urlBuilder.toString();
    }

    private void checkIfResponseIsSuccessful(HttpURLConnection connection) throws IOException {
        final int responseCode = connection.getResponseCode();
        if (responseCode != HttpConstants.STATUS_CODES.OK) {
            // FIXME read error body
            throw new HttpException(responseCode, null);
        }
    }

    private void checkIfContentTypeIsJSON(HttpURLConnection connection) {
        String contentType = connection.getHeaderField(HttpConstants.HEADERS.CONTENT_TYPE);
        if (contentType == null || !(contentType.equals("application/json") || contentType.equals("application/json;charset=UTF-8"))) {
            throw new RuntimeException(HttpConstants.HEADERS.CONTENT_TYPE + " " + contentType + " is not supported");
        }
    }

    private boolean jsonResponseBodyNeedsToBeRead(HttpURLConnection connection) {
        String contentType = connection.getHeaderField(HttpConstants.HEADERS.CONTENT_TYPE);

        String contentLength = connection.getHeaderField(HttpConstants.HEADERS.CONTENT_LENGTH); // body is sent in non-stream mode
        String transferEncodingType = connection.getHeaderField(HttpConstants.HEADERS.TRANSFER_ENCODING); // stream mode, see https://en.wikipedia.org/wiki/Chunked_transfer_encoding

        final boolean isContentTypeJSON = contentType != null && (contentType.equals("application/json") || contentType.equals("application/json;charset=UTF-8"));
        final boolean isBodyTransferTypePresent = contentLength != null || transferEncodingType != null;

        return isContentTypeJSON && isBodyTransferTypePresent;
    }

    private String readBody(HttpURLConnection connection) throws IOException {
        StringBuilder response = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
            }
            return response.toString();
        }
    }

    private Optional<String> doRequestWithBody(URL url, String method, @Nullable Jsonable jsonBody) throws Exception {
        // connection holder just automatically closes connection
        try (HttpURLConnectionHolder httpURLConnectionHolder = new HttpURLConnectionHolder((HttpURLConnection) url.openConnection())) {
            // response is optional, based on content-length and content-type
            Optional<String> optionalResult = Optional.empty();

            // set authentication and other addition information provided by interceptor
            HttpURLConnection connection = httpURLConnectionHolder.getConnection();
            connectionInterceptionManager.executeAllBeforeConnectingInterceptors(connection);

            // overwrite settings which possible were set by interceptor
            connection.setRequestMethod(method);
            connection.setRequestProperty(HttpConstants.HEADERS.ACCEPT_LANGUAGE, "UTF-8");
            connection.setInstanceFollowRedirects(false); // REST Api

            // check whether payload needs to be sent
            if (jsonBody != null) {
                connection.setRequestProperty(HttpConstants.HEADERS.CONTENT_TYPE, "application/json"); // support only for json
                connection.setDoOutput(true);

                try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream())) {
                    outputStreamWriter.write(jsonBody.toJSONObject().toString());
                    outputStreamWriter.flush();
                }
            }

            checkIfResponseIsSuccessful(connection);

            // read json body if present
            if (jsonResponseBodyNeedsToBeRead(connection)) {
                optionalResult = Optional.of(readBody(connection));
            }

            return optionalResult;
        }
    }

    private String doGET(URL url) throws Exception {
        try (HttpURLConnectionHolder httpURLConnectionHolder = new HttpURLConnectionHolder((HttpURLConnection) url.openConnection())) {
            // set authentication and other addition information provided by interceptor
            HttpURLConnection connection = httpURLConnectionHolder.getConnection();
            connectionInterceptionManager.executeAllBeforeConnectingInterceptors(connection);

            // overwrite settings which possible were set by interceptor
            connection.setRequestMethod(HttpConstants.METHODS.GET);
            connection.setRequestProperty(HttpConstants.HEADERS.ACCEPT_LANGUAGE, "UTF-8");
            connection.setInstanceFollowRedirects(false);

            checkIfResponseIsSuccessful(connection);
            checkIfContentTypeIsJSON(connection); // in this app only json body is supported

            return readBody(connection);
        }
    }

    public String get(String url, @Nullable Map<String, String> queryParams) throws Exception {
        return doGET(new URL(queryParams != null ? appendQueryParams(url, queryParams) : url));
    }

    public Optional<String> post(String url, @Nullable Map<String, String> queryParams, @Nullable Jsonable jsonBody) throws Exception {
        return doRequestWithBody(new URL(queryParams != null ? appendQueryParams(url, queryParams) : url), HttpConstants.METHODS.POST, jsonBody);
    }

    public Optional<String> put(String url, @Nullable Map<String, String> queryParams, @Nullable Jsonable jsonBody) throws Exception {
        return doRequestWithBody(new URL(queryParams != null ? appendQueryParams(url, queryParams) : url), HttpConstants.METHODS.PUT, jsonBody);
    }

    public Optional<String> delete(String url, @Nullable Map<String, String> queryParams, @Nullable Jsonable jsonBody) throws Exception {
        return doRequestWithBody(new URL(queryParams != null ? appendQueryParams(url, queryParams) : url), HttpConstants.METHODS.DELETE, jsonBody);
    }
}