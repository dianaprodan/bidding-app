package eu.ase.bidding.persistence;

import org.json.JSONException;
import org.json.JSONObject;

public interface Jsonable {
    JSONObject toJSONObject() throws JSONException;

    Jsonable fromJSONObject(JSONObject json) throws JSONException;
}
