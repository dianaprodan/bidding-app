package eu.ase.bidding.persistence;

import org.xmlpull.v1.XmlSerializer;

public interface Xmlable {
    void serialize(XmlSerializer serializer);
}
