package eu.ase.bidding.persistence;

import android.content.ContentValues;

public interface Persistable {
    // TODO could be any others overloads, depending on used storage

    ContentValues toContentValues();
}
