package eu.ase.bidding.persistence;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

public class SerializableBitmap implements Serializable {

    private Bitmap bitmap;
    private byte[] compressedBitmap;

    public SerializableBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        this.compressedBitmap = null;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        if (compressedBitmap == null) {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
            compressedBitmap = byteStream.toByteArray();
        }
        out.write(this.compressedBitmap, 0, this.compressedBitmap.length);
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int b;
        while ((b = in.read()) != -1) {
            byteStream.write(b);
        }
        byte[] bitmapBytes = byteStream.toByteArray();
        bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
    }
}
