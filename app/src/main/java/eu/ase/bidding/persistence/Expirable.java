package eu.ase.bidding.persistence;

public interface Expirable {
    boolean expired();
}
