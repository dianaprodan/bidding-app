package eu.ase.bidding.models.dto.in;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import eu.ase.bidding.persistence.Jsonable;
import eu.ase.bidding.utils.ConverterUtils;

public class AuctionBidDTO implements Jsonable {
    private static final String ID_JSON_KEY = "id";
    private static final String BIDDER_ID_JSON_KEY = "bidderId";
    private static final String AUCTION_ITEM_ID_JSON_KEY = "auctionItemId";
    private static final String AMOUNT_JSON_KEY = "amount";
    private static final String CREATED_AT_JSON_KEY = "createdAt";


    public Long id;
    public String bidderId;
    public Long auctionItemId;
    public Double amount;
    public Date createdAt;

    AuctionBidDTO() {
    }

    @Override
    public JSONObject toJSONObject() {
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.id = json.getLong(ID_JSON_KEY);
        this.bidderId = json.getString(BIDDER_ID_JSON_KEY);
        this.auctionItemId = json.getLong(AUCTION_ITEM_ID_JSON_KEY);
        this.amount = json.getDouble(AMOUNT_JSON_KEY);
        this.createdAt = ConverterUtils.dateFromISOString(json.getString(CREATED_AT_JSON_KEY));
        return this;
    }
}
