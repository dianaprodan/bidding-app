package eu.ase.bidding.models.dto.in;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.persistence.Jsonable;

public class ResourceIdResponse implements Jsonable {
    private static final String ID_JSON_KEY = "id";

    public Long id;

    public ResourceIdResponse() {
    }

    @Override
    public JSONObject toJSONObject() {
        throw new RuntimeException("NOT IMPL");
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.id = json.getLong(ID_JSON_KEY);
        return this;
    }
}
