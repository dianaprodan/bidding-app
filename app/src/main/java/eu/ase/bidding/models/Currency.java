package eu.ase.bidding.models;

import java.io.Serializable;

public enum Currency implements Serializable {EUR, USD}
