package eu.ase.bidding.models.dto.in;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eu.ase.bidding.persistence.Jsonable;

public class AuctionItemsResponse implements Jsonable {
    private static final String ITEMS_JSON_KEY = "items";

    public List<AuctionItemDTO> items;

    @Override
    public JSONObject toJSONObject() {
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.items = new ArrayList<>();
        JSONArray jsonArray = json.getJSONArray(ITEMS_JSON_KEY);
        for (int i = 0; i < jsonArray.length(); i++) {
            this.items.add((AuctionItemDTO) new AuctionItemDTO().fromJSONObject(jsonArray.getJSONObject(i)));
        }
        return this;
    }
}
