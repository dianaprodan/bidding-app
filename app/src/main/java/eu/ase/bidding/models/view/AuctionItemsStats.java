package eu.ase.bidding.models.view;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.persistence.Jsonable;

public class AuctionItemsStats implements Jsonable, Serializable {
    private final String PERIOD_JSON_KEY = "period";
    private final String STATUS_STATS_JSON_KEY = "statusStats";

    public String period;
    public Map<AuctionItemStatus.Status, AuctionItemStatusStats> statusStats;

    public AuctionItemsStats() {
    }

    public AuctionItemsStats(String period, Map<AuctionItemStatus.Status, AuctionItemStatusStats> statusStats) {
        this.period = period;
        this.statusStats = statusStats;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(PERIOD_JSON_KEY, period);
        JSONObject statusStats = new JSONObject();
        for (Map.Entry<AuctionItemStatus.Status, AuctionItemStatusStats> entry : this.statusStats.entrySet()) {
            statusStats.put(entry.getKey().toString(), entry.getValue().toJSONObject());
        }
        json.put(STATUS_STATS_JSON_KEY, statusStats);
        return json;
    }

    @Override
    public AuctionItemsStats fromJSONObject(JSONObject json) throws JSONException {
        this.period = json.getString(PERIOD_JSON_KEY);
        this.statusStats = new HashMap<>();
        JSONObject statusStats = json.getJSONObject(STATUS_STATS_JSON_KEY);
        final Iterator<String> iterator = statusStats.keys();
        while (iterator.hasNext()) {
            String status = iterator.next();
            this.statusStats.put(AuctionItemStatus.Status.valueOf(status), new AuctionItemStatusStats().fromJSONObject(statusStats.getJSONObject(status)));
        }
        return this;
    }

    public static class AuctionItemStatusStats implements Jsonable, Serializable {
        private final String COUNT_JSON_KEY = "count";
        private final String AMOUNT_JSON_KEY = "amount";

        public int count;
        public Double amount;

        AuctionItemStatusStats() {
        }

        public AuctionItemStatusStats(int count, Double amount) {
            this.count = count;
            this.amount = amount;
        }

        @Override
        @NonNull
        public String toString() {
            return String.format(Locale.ENGLISH, "%d items (%.2f EUR)", count, amount);
        }

        @Override
        public JSONObject toJSONObject() throws JSONException {
            JSONObject json = new JSONObject();
            json.put(COUNT_JSON_KEY, count);
            json.put(AMOUNT_JSON_KEY, amount);
            return json;
        }

        @Override
        public AuctionItemStatusStats fromJSONObject(JSONObject json) throws JSONException {
            this.count = json.getInt(COUNT_JSON_KEY);
            this.amount = json.getDouble(AMOUNT_JSON_KEY);
            return this;
        }
    }
}
