package eu.ase.bidding.models.entities;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

import eu.ase.bidding.models.dto.in.AuctionBidDTO;
import eu.ase.bidding.models.dto.in.ResourceIdResponse;
import eu.ase.bidding.persistence.Persistable;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_BID_CONTRACT;
import eu.ase.bidding.utils.SQLiteUtils;

public class AuctionBid implements Serializable, Persistable, Comparable<AuctionBid> {
    private final Long id;
    private final String bidderId;
    private final Long auctionItemId;
    private final Double amount;
    private final Date timestamp;

    AuctionBid(AuctionBidDTO auctionBidDTO) {
        this.id = auctionBidDTO.id;
        this.bidderId = auctionBidDTO.bidderId;
        this.auctionItemId = auctionBidDTO.auctionItemId;
        this.amount = auctionBidDTO.amount;
        this.timestamp = auctionBidDTO.createdAt;
    }

    public AuctionBid(ResourceIdResponse resourceIdResponse, String bidderId, Long auctionItemId, Double amount) {
        this.id = resourceIdResponse.id;
        this.bidderId = bidderId;
        this.auctionItemId = auctionItemId;
        this.amount = amount;
        this.timestamp = new Date();
    }

    @SuppressWarnings("unused")
    public AuctionBid(Cursor cursor) {
        this.id = SQLiteUtils.getLongFrom(cursor, AUCTION_BID_CONTRACT.ENTRY._ID);
        this.bidderId = SQLiteUtils.getStringFrom(cursor, AUCTION_BID_CONTRACT.ENTRY.BIDDER_ID);
        this.auctionItemId = SQLiteUtils.getLongFrom(cursor, AUCTION_BID_CONTRACT.ENTRY.AUCTION_ITEM_ID);
        this.amount = SQLiteUtils.getDoubleFrom(cursor, AUCTION_BID_CONTRACT.ENTRY.AMOUNT);
        this.timestamp = new Date(SQLiteUtils.getLongFrom(cursor, AUCTION_BID_CONTRACT.ENTRY.TIMESTAMP));
    }

    public Double getAmount() {
        return amount;
    }

    public String getBidderId() {
        return bidderId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AUCTION_BID_CONTRACT.ENTRY._ID, this.id);
        contentValues.put(AUCTION_BID_CONTRACT.ENTRY.BIDDER_ID, this.bidderId);
        contentValues.put(AUCTION_BID_CONTRACT.ENTRY.AUCTION_ITEM_ID, this.auctionItemId);
        contentValues.put(AUCTION_BID_CONTRACT.ENTRY.AMOUNT, this.amount);
        contentValues.put(AUCTION_BID_CONTRACT.ENTRY.TIMESTAMP, this.timestamp.getTime());
        return contentValues;
    }

    @Override
    public int compareTo(@NonNull AuctionBid other) {
        return this.timestamp.compareTo(other.timestamp);
    }
}
