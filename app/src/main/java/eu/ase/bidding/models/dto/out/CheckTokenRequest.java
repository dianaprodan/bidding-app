package eu.ase.bidding.models.dto.out;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.persistence.Jsonable;

public class CheckTokenRequest implements Jsonable {
    private static final String TOKEN_JSON_KEY = "token";

    private String token;

    public CheckTokenRequest(String token) {
        this.token = token;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(TOKEN_JSON_KEY, token);
        return jsonObject;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.token = json.getString(TOKEN_JSON_KEY);
        return this;
    }
}
