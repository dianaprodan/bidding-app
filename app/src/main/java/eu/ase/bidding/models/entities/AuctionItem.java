package eu.ase.bidding.models.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import eu.ase.bidding.config.Models;
import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.Coordinate;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.models.dto.in.AuctionBidDTO;
import eu.ase.bidding.models.dto.in.AuctionItemDTO;
import eu.ase.bidding.models.dto.in.ResourceIdResponse;
import eu.ase.bidding.persistence.Expirable;
import eu.ase.bidding.persistence.Persistable;
import eu.ase.bidding.persistence.SerializableBitmap;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_ITEM_CONTRACT;
import eu.ase.bidding.repository.sqlite3.contracts.USER_CONTRACT;
import eu.ase.bidding.utils.ImageUtils;
import eu.ase.bidding.utils.SQLiteUtils;
import eu.ase.bidding.utils.StringUtils;

public class AuctionItem implements Serializable, Persistable, Expirable {
    private final Date createdAt;
    private final Long id;
    private String name;
    private User owner;
    private String description;
    private final Rating rating;
    private Currency currency;
    private Double startingPrice;
    private SerializableBitmap serializableImage;
    private AuctionItemStatus currentStatus;
    private final Coordinate coordinate;
    private List<AuctionBid> bids;

    @SuppressWarnings("unused")
    public AuctionItem(Cursor cursor) {
        this.id = SQLiteUtils.getLongFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY._ID);
        this.name = cursor.getString(1); // name for auction item
        this.description = SQLiteUtils.getStringFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.DESCRIPTION);
        this.rating = new Rating(StringUtils.splitIntoFloats(SQLiteUtils.getStringFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.RATING), ","));
        this.currency = Currency.valueOf(SQLiteUtils.getStringFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.CURRENCY));
        this.startingPrice = SQLiteUtils.getDoubleFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.STARTING_PRICE);
        this.serializableImage = new SerializableBitmap(ImageUtils.convert(SQLiteUtils.getStringFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.IMAGE)));
        this.currentStatus = new AuctionItemStatus(cursor);
        this.coordinate = new Coordinate(SQLiteUtils.getDoubleFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.COORD_LAT),
                SQLiteUtils.getDoubleFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.COORD_LONG));
        this.bids = new ArrayList<>();
        this.createdAt = SQLiteUtils.getTimestampFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.STORED_AT);
        this.owner = new User(SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.USERNAME), cursor.getString(15),
                SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.ADDRESS), SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.EMAIL),
                SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.MOBILE_NUMBER));
    }

    public AuctionItem(AuctionItemDTO auctionItemDTO) {
        this.id = auctionItemDTO.id;
        this.setName(auctionItemDTO.name);
        this.setOwner(new User(auctionItemDTO.owner));
        this.setDescription(auctionItemDTO.description);
        this.rating = new Rating(StringUtils.splitIntoFloats(auctionItemDTO.rating, ","));
        this.setCurrency(auctionItemDTO.currency);
        this.setStartingPrice(auctionItemDTO.startingPrice);
        this.setImage(ImageUtils.convert(auctionItemDTO.image));
        this.currentStatus = auctionItemDTO.currentStatus;
        this.coordinate = auctionItemDTO.coordinate;
        this.bids = new ArrayList<>();
        for (AuctionBidDTO auctionBidDTO : auctionItemDTO.bids) {
            this.bids.add(new AuctionBid(auctionBidDTO));
        }
        Collections.sort(bids);
        this.createdAt = new Date();
    }

    public AuctionItem(ResourceIdResponse resourceIdResponse, User owner, List<AuctionBid> bids,
                       eu.ase.bidding.models.dto.out.AuctionItemDTO auctionItemDTO) {
        this.id = resourceIdResponse.id;
        this.setName(auctionItemDTO.name);
        this.setOwner(owner);
        this.setDescription(auctionItemDTO.description);
        this.rating = new Rating(StringUtils.splitIntoFloats(auctionItemDTO.rating, ","));
        this.setCurrency(auctionItemDTO.currency);
        this.setStartingPrice(auctionItemDTO.startingPrice);
        this.setImage(ImageUtils.convert(auctionItemDTO.image));
        this.currentStatus = new AuctionItemStatus();
        this.coordinate = auctionItemDTO.coordinate;
        this.bids = bids;
        Collections.sort(bids);
        this.createdAt = new Date();
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY._ID, this.id);
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.NAME, this.name);
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.OWNER, this.owner.getUsername());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.DESCRIPTION, this.description);
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.RATING, this.rating.stringifyRating());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.CURRENCY, this.currency.toString());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.STARTING_PRICE, this.startingPrice);
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.IMAGE, ImageUtils.convert(this.serializableImage.getBitmap()));
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.CURRENT_STATUS, this.currentStatus.status.toString());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.TIMESTAMP_FOR_STATUS, this.currentStatus.timestamp.getTime());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.STORED_AT, this.createdAt.getTime());
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.COORD_LAT, this.coordinate.latitude);
        contentValues.put(AUCTION_ITEM_CONTRACT.ENTRY.COORD_LONG, this.coordinate.longitude);
        return contentValues;
    }

    @Override
    public boolean expired() {
        return this.createdAt.getTime() + Models.TTL.AUCTION_ITEM < new Date().getTime();
    }

    public Coordinate getCoordinate() { return coordinate; }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        if (!Validator.validateName(name)) {
            throw new RuntimeException("Invalid Name");
        }
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    private void setOwner(@NonNull User owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(@NonNull String description) {
        if (!Validator.validateDescription(description)) {
            throw new RuntimeException("Invalid description");
        }
        this.description = description;
    }

    public Float getRating() {
        return this.rating.getRating();
    }

    public String ratingToString() {
        return this.rating.stringifyRating();
    }

    public void addStars(Float stars) {
        this.rating.addStars(stars);
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    private void setStartingPrice(Double startingPrice) {
        if (!Validator.validateStartingPrice(startingPrice)) {
            throw new RuntimeException("Invalid starting price");
        }
        this.startingPrice = startingPrice;
    }

    public @Nullable
    AuctionBid getLastBid() {
        AuctionBid lastBid = null;
        if (!this.bids.isEmpty()) {
            lastBid = this.bids.get(this.bids.size() - 1);
        }
        return lastBid;
    }

    public Double getCurrentPrice() {
        final AuctionBid lastBid = this.getLastBid();
        return lastBid != null ? lastBid.getAmount() : startingPrice;
    }

    public String currentPriceToString() {
        Double currentPrice = this.getCurrentPrice();
        return currentPrice.toString() + " " + this.currency.toString();
    }

    public Date getLastBidTimestamp() {
        final AuctionBid lastBid = this.getLastBid();
        return lastBid != null ? lastBid.getTimestamp() : null;
    }

    public Bitmap getImage() {
        return serializableImage.getBitmap();
    }

    private void setImage(Bitmap image) {
        this.serializableImage = new SerializableBitmap(image);
    }

    public AuctionItemStatus getCurrentStatus() {
        return this.currentStatus;
    }

    public void changeCurrentStatus() {
        this.currentStatus = AuctionItemStatus.nextStateFrom(this.currentStatus);
    }

    public List<AuctionBid> getBids() {
        return bids;
    }

    public void addBid(AuctionBid bid) {
        this.bids.add(bid);
    }

    public void addBids(List<AuctionBid> bids) {
        this.bids.addAll(bids);
        Collections.sort(this.bids);
    }

    public enum Owner {
        USER("@me") {
            @Override
            public String getSelectSQL() {
                return AUCTION_ITEM_CONTRACT.SQL.GET_FOR_CURRENT_USER;
            }
        },
        OTHERS("@others") {
            @Override
            public String getSelectSQL() {
                return AUCTION_ITEM_CONTRACT.SQL.GET_FOR_OTHER_USERS;
            }
        };

        private final String owner;

        Owner(String s) {
            owner = s;
        }

        public String getOwner() {
            return owner;
        }

        public abstract String getSelectSQL();
    }

    public static class Validator {
        public static boolean validateName(String name) {
            return (name.length() > 2 && name.length() <= 15);
        }

        public static boolean validateDescription(String description) {
            return (description.length() > 2 && description.length() <= 50);
        }

        public static boolean validateStartingPrice(Double price) {
            return price > 0;
        }
    }
}

class Rating implements Serializable {
    private List<Float> stars;
    private Float rating;

    Rating(List<Float> stars) {
        this.stars = stars;
        this.update();
    }

    void addStars(Float stars) {
        if (stars < 0 || stars > 5) {
            throw new RuntimeException("Invalid number of stars");
        }
        this.stars.add(stars);
    }

    private void update() {
        float sum = 0;
        for (int i = 0; i < this.stars.size(); i++) {
            sum += this.stars.get(i);
        }
        this.rating = sum / this.stars.size();
    }

    Float getRating() {
        update();
        return this.rating;
    }

    String stringifyRating() {
        return StringUtils.join(stars, ",");
    }
}