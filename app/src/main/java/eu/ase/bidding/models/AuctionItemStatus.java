package eu.ase.bidding.models;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

import eu.ase.bidding.persistence.Jsonable;
import eu.ase.bidding.repository.sqlite3.contracts.AUCTION_ITEM_CONTRACT;
import eu.ase.bidding.utils.ConverterUtils;
import eu.ase.bidding.utils.SQLiteUtils;

public class AuctionItemStatus implements Serializable, Jsonable {
    private static final String STATUS_JSON_KEY = "status";
    private static final String TIMESTAMP_JSON_KEY = "timestamp";

    public final Status status;
    public final Date timestamp;

    public AuctionItemStatus() {
        this.status = Status.CREATED;
        this.timestamp = new Date();
    }

    private AuctionItemStatus(Status status) {
        this.status = status;
        this.timestamp = new Date();
    }

    public AuctionItemStatus(JSONObject json) throws JSONException {
        this.status = Status.valueOf(json.getString(STATUS_JSON_KEY));
        this.timestamp = ConverterUtils.dateFromISOString(json.getString(TIMESTAMP_JSON_KEY));
    }

    public AuctionItemStatus(Cursor cursor) {
        this.status = AuctionItemStatus.Status.valueOf(SQLiteUtils.getStringFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.CURRENT_STATUS));
        this.timestamp = SQLiteUtils.getTimestampFrom(cursor, AUCTION_ITEM_CONTRACT.ENTRY.TIMESTAMP_FOR_STATUS);
    }

    public static AuctionItemStatus nextStateFrom(AuctionItemStatus currentStatus) {
        return new AuctionItemStatus(currentStatus.status.nextStatus());
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(STATUS_JSON_KEY, status.toString());
        json.put(TIMESTAMP_JSON_KEY, timestamp.getTime());
        return json;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        return new AuctionItemStatus(json);
    }

    public enum Status implements Serializable {
        CREATED {
            @Override
            public Status nextStatus() {
                return Status.IN_AUCTION;
            }
        },
        IN_AUCTION {
            @Override
            public Status nextStatus() {
                return Status.SOLD_OUT;
            }
        },
        SOLD_OUT {
            @Override
            public Status nextStatus() {
                throw new RuntimeException("Final state");
            }
        };

        public abstract Status nextStatus();
    }
}
