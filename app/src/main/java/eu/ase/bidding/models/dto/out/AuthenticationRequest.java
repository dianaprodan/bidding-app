package eu.ase.bidding.models.dto.out;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.persistence.Jsonable;

public class AuthenticationRequest implements Jsonable {
    private static final String USERNAME_JSON_KEY = "username";
    private static final String PASSWORD_JSON_KEY = "password";

    private final String username;
    private final String password;

    public AuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(USERNAME_JSON_KEY, username);
        jsonObject.put(PASSWORD_JSON_KEY, password);
        return jsonObject;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) {
        throw new RuntimeException("NOT IMPLEMENTED");
    }
}
