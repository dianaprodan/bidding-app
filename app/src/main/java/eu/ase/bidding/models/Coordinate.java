package eu.ase.bidding.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import eu.ase.bidding.persistence.Jsonable;

public class Coordinate implements Serializable, Jsonable {
    private static final String LATITUDE_JSON_KEY = "latitude";
    private static final String LONGITUDE_JSON_KEY = "longitude";

    public final Double latitude;
    public final Double longitude;

    public Coordinate(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coordinate(JSONObject jsonObject) throws JSONException {
        this.latitude = jsonObject.getDouble(LATITUDE_JSON_KEY);
        this.longitude = jsonObject.getDouble(LONGITUDE_JSON_KEY);
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(LATITUDE_JSON_KEY, latitude);
        jsonObject.put(LONGITUDE_JSON_KEY, longitude);
        return jsonObject;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) {
        throw new RuntimeException("NOT IMPLEMENTED");
    }
}
