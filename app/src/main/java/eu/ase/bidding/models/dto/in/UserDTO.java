package eu.ase.bidding.models.dto.in;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.persistence.Jsonable;

public class UserDTO implements Jsonable {
    private static final String USERNAME_JSON_KEY = "username";
    private static final String NAME_JSON_KEY = "name";
    private static final String ADDRESS_JSON_KEY = "address";
    private static final String EMAIL_JSON_KEY = "email";
    private static final String MOBILE_NUMBER_JSON_KEY = "mobileNumber";

    public String username;
    public String name;
    public String address;
    public String email;
    public String mobileNumber;

    UserDTO() {
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(USERNAME_JSON_KEY, this.username);
        json.put(NAME_JSON_KEY, this.name);
        json.put(ADDRESS_JSON_KEY, this.address);
        json.put(EMAIL_JSON_KEY, this.email);
        json.put(MOBILE_NUMBER_JSON_KEY, this.mobileNumber);
        return json;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.username = json.getString(USERNAME_JSON_KEY);
        this.name = json.getString(NAME_JSON_KEY);
        this.address = json.getString(ADDRESS_JSON_KEY);
        this.email = json.getString(EMAIL_JSON_KEY);
        this.mobileNumber = json.getString(MOBILE_NUMBER_JSON_KEY);
        return this;
    }
}
