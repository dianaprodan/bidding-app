package eu.ase.bidding.models.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import eu.ase.bidding.models.dto.in.UserDTO;
import eu.ase.bidding.persistence.Jsonable;
import eu.ase.bidding.persistence.Persistable;
import eu.ase.bidding.repository.sqlite3.contracts.USER_CONTRACT;
import eu.ase.bidding.utils.CryptoUtils;
import eu.ase.bidding.utils.SQLiteUtils;

public class User implements Serializable, Persistable, Jsonable {
    private static final String COMPUTED_ID_JSON_KEY = "computedId";
    private static final String USERNAME_JSON_KEY = "username";
    private static final String NAME_JSON_KEY = "name";
    private static final String ADDRESS_JSON_KEY = "address";
    private static final String EMAIL_JSON_KEY = "email";
    private static final String MOBILE_NUMBER_JSON_KEY = "mobileNumber";

    private @Nullable
    String computedId = null;
    private String username;
    private String name;
    private String address;
    private String email;
    private String mobileNumber;

    /**
     * To be used only to be able to construct Users from JSONs
     * DO NOT USE IT ALONE!
     */
    public User() {
        this.username = null;
        this.name = null;
        this.address = null;
        this.email = null;
        this.mobileNumber = null;
    }

    User(String username, String name, String address, String email, String mobileNumber) {
        this.username = username;
        this.name = name;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
    }

    @SuppressWarnings("unused")
    public User(Cursor cursor) {
        this.username = SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.USERNAME);
        this.name = SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.NAME);
        this.address = SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.ADDRESS);
        this.email = SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.EMAIL);
        this.mobileNumber = SQLiteUtils.getStringFrom(cursor, USER_CONTRACT.ENTRY.MOBILE_NUMBER);
    }

    public User(UserDTO userDTO) {
        this.username = userDTO.username;
        this.name = userDTO.name;
        this.address = userDTO.address;
        this.email = userDTO.email;
        this.mobileNumber = userDTO.mobileNumber;
    }

    public User(eu.ase.bidding.models.dto.out.UserDTO userDTO) {
        this.username = userDTO.username;
        this.name = userDTO.name;
        this.address = userDTO.address;
        this.email = userDTO.email;
        this.mobileNumber = userDTO.mobileNumber;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_CONTRACT.ENTRY.USERNAME, this.username);
        contentValues.put(USER_CONTRACT.ENTRY.NAME, this.name);
        contentValues.put(USER_CONTRACT.ENTRY.ADDRESS, this.address);
        contentValues.put(USER_CONTRACT.ENTRY.EMAIL, this.email);
        contentValues.put(USER_CONTRACT.ENTRY.MOBILE_NUMBER, this.mobileNumber);
        return contentValues;
    }

    public void intoTextView(TextView textView) {
        textView.setText(this.name);
    }

    public String getUsername() {
        return username;
    }

    public String getId() throws NoSuchAlgorithmException {
        if (computedId == null) {
            computedId = CryptoUtils.md5(username);
        }
        return computedId;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        if (computedId != null) {
            json.put(COMPUTED_ID_JSON_KEY, this.computedId);
        }
        json.put(USERNAME_JSON_KEY, this.username);
        json.put(NAME_JSON_KEY, this.name);
        json.put(ADDRESS_JSON_KEY, this.address);
        json.put(EMAIL_JSON_KEY, this.email);
        json.put(MOBILE_NUMBER_JSON_KEY, this.mobileNumber);
        return json;
    }

    @Override
    public User fromJSONObject(JSONObject json) throws JSONException {
        User user = new User();
        if (json.has(COMPUTED_ID_JSON_KEY)) {
            user.computedId = json.getString(COMPUTED_ID_JSON_KEY);
        }
        user.username = json.getString(USERNAME_JSON_KEY);
        user.name = json.getString(NAME_JSON_KEY);
        user.address = json.getString(ADDRESS_JSON_KEY);
        user.email = json.getString(EMAIL_JSON_KEY);
        user.mobileNumber = json.getString(MOBILE_NUMBER_JSON_KEY);
        return user;
    }
}
