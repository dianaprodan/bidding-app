package eu.ase.bidding.models.dto.out;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.models.Coordinate;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.persistence.Jsonable;

public class AuctionItemDTO implements Jsonable {
    private final static String NAME_JSON_KEY = "name";
    private final static String DESCRIPTION_JSON_KEY = "description";
    private final static String RATING_JSON_KEY = "rating";
    private final static String CURRENCY_JSON_KEY = "currency";
    private final static String STARTING_PRICE_JSON_KEY = "startingPrice";
    private final static String IMAGE_JSON_KEY = "image";
    private final static String COORDINATE_JSON_KEY = "coordinate";

    public String name;
    public String description;
    public String rating;
    public Currency currency;
    public Double startingPrice;
    public String image;
    public Coordinate coordinate;

    public AuctionItemDTO(String name, String description, String rating, Currency currency,
                          Double startingPrice, String image, Coordinate coordinate) {
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.currency = currency;
        this.startingPrice = startingPrice;
        this.image = image;
        this.coordinate = coordinate;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(NAME_JSON_KEY, this.name);
        json.put(DESCRIPTION_JSON_KEY, this.description);
        json.put(RATING_JSON_KEY, this.rating);
        json.put(CURRENCY_JSON_KEY, this.currency);
        json.put(STARTING_PRICE_JSON_KEY, this.startingPrice);
        json.put(IMAGE_JSON_KEY, this.image);
        json.put(COORDINATE_JSON_KEY, this.coordinate.toJSONObject());
        return json;
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) {
        throw new RuntimeException("NOT IMPLEMENTED");
    }
}
