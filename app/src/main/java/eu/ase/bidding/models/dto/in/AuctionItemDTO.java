package eu.ase.bidding.models.dto.in;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eu.ase.bidding.models.AuctionItemStatus;
import eu.ase.bidding.models.Coordinate;
import eu.ase.bidding.models.Currency;
import eu.ase.bidding.persistence.Jsonable;

public class AuctionItemDTO implements Jsonable {
    private final static String ID_JSON_KEY = "id";
    private final static String NAME_JSON_KEY = "name";
    private final static String OWNER_JSON_KEY = "owner";
    private final static String DESCRIPTION_JSON_KEY = "description";
    private final static String RATING_JSON_KEY = "rating";
    private final static String CURRENCY_JSON_KEY = "currency";
    private final static String STARTING_PRICE_JSON_KEY = "startingPrice";
    private final static String IMAGE_JSON_KEY = "image";
    private final static String CURRENT_STATUS_JSON_KEY = "currentStatus";
    private final static String COORDINATE_JSON_KEY = "coordinate";
    private final static String BIDS_JSON_KEY = "bids";

    public Long id;
    public String name;
    public UserDTO owner;
    public String description;
    public String rating;
    public Currency currency;
    public Double startingPrice;
    public String image;
    public AuctionItemStatus currentStatus;
    public Coordinate coordinate;
    public List<AuctionBidDTO> bids;

    AuctionItemDTO() {
    }

    @Override
    public JSONObject toJSONObject() {
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.id = json.getLong(ID_JSON_KEY);
        this.name = json.getString(NAME_JSON_KEY);
        this.owner = (UserDTO) new UserDTO().fromJSONObject(json.getJSONObject(OWNER_JSON_KEY));
        this.description = json.getString(DESCRIPTION_JSON_KEY);
        this.rating = json.getString(RATING_JSON_KEY);
        this.currency = Currency.valueOf(json.getString(CURRENCY_JSON_KEY));
        this.startingPrice = json.getDouble(STARTING_PRICE_JSON_KEY);
        this.image = json.getString(IMAGE_JSON_KEY);
        this.currentStatus = new AuctionItemStatus(json.getJSONObject(CURRENT_STATUS_JSON_KEY));
        this.coordinate = new Coordinate(json.getJSONObject(COORDINATE_JSON_KEY));
        this.bids = new ArrayList<>();
        JSONArray bids = json.getJSONArray(BIDS_JSON_KEY);
        for (int i = 0; i < bids.length(); i++) {
            this.bids.add((AuctionBidDTO) new AuctionBidDTO().fromJSONObject(bids.getJSONObject(i)));
        }
        return this;
    }

}
