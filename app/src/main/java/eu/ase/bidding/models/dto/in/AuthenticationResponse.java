package eu.ase.bidding.models.dto.in;

import org.json.JSONException;
import org.json.JSONObject;

import eu.ase.bidding.persistence.Jsonable;

public class AuthenticationResponse implements Jsonable {
    private static final String JWT_JSON_KEY = "jwt";
    private static final String USER_DTO_JSON_KEY = "user";

    public String jwt;
    public UserDTO user;

    public AuthenticationResponse() {
    }

    @Override
    public JSONObject toJSONObject() {
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @Override
    public Jsonable fromJSONObject(JSONObject json) throws JSONException {
        this.jwt = json.getString(JWT_JSON_KEY);
        this.user = (UserDTO) new UserDTO().fromJSONObject(json.getJSONObject(USER_DTO_JSON_KEY));
        return this;
    }
}
