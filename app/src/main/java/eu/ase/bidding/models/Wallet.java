package eu.ase.bidding.models;

import androidx.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import eu.ase.bidding.config.Models;
import eu.ase.bidding.persistence.Jsonable;

@IgnoreExtraProperties
public class Wallet implements Jsonable {
    public static final String BALANCE_FIREBASE_JSON_KEY = "balance";
    private static final String BALANCE_JSON_KEY = "balance";
    private Map<String, Double> balance = new HashMap<>();

    public Wallet() {
        for (Currency currency : Currency.values()) {
            balance.put(currency.toString(), Models.WALLET_INITIAL_BALANCE); // initial amount of money
        }
    }

    @SuppressWarnings("unused")
    public Wallet(JSONObject json) throws JSONException {
        JSONObject balanceJson = json.getJSONObject(BALANCE_JSON_KEY);
        Iterator<String> keysIterator = balanceJson.keys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            balance.put(key, balanceJson.getDouble(key));
        }
    }

    @SuppressWarnings("unused")
    public Map<String, Double> getBalance() {
        return balance;
    }

    public @NonNull
    Double getBalance(Currency currency) {
        return Objects.requireNonNull(balance.get(currency.toString()),
                "No available balance for this type of currency " + currency);
    }

    public boolean has(Double amount, Currency currency) {
        Double availableBalance = balance.get(currency.toString());
        return availableBalance != null && availableBalance >= amount;
    }

    public Transaction withdraw(Double amount, Currency currency) {
        String currencyString = currency.toString();

        Double availableBalance = Objects.requireNonNull(balance.get(currencyString));

        availableBalance = availableBalance - amount;
        if (availableBalance < 0) {
            throw new RuntimeException("No available balance for this type of currency " + currencyString);
        }

        return new Transaction(currencyString, availableBalance);
    }

    public Transaction deposit(Double amount, Currency currency) {
        String currencyString = currency.toString();
        Double availableBalance = balance.get(currencyString);
        return new Transaction(currencyString, availableBalance != null ? availableBalance + amount : amount);
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject balanceJson = new JSONObject();
        for (Map.Entry<String, Double> entry : balance.entrySet()) {
            balanceJson.put(entry.getKey(), entry.getValue().doubleValue());
        }
        json.put(BALANCE_JSON_KEY, balanceJson);
        return json;
    }

    @Override
    public Wallet fromJSONObject(JSONObject json) throws JSONException {
        Wallet wallet = new Wallet();
        JSONObject balanceJson = json.getJSONObject(BALANCE_JSON_KEY);
        Iterator<String> keysIterator = balanceJson.keys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            wallet.balance.put(key, balanceJson.getDouble(key));
        }
        return wallet;
    }

    public class Transaction {
        public final String currency;
        public final Double newAmount;

        private Transaction(String currency, Double newAmount) {
            this.currency = currency;
            this.newAmount = newAmount;
        }

        public void commit() {
            balance.put(currency, newAmount);
        }
    }
}
